package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class Payment {

    public model.Payment create(model.Account bank, double amount) {
        model.Payment payment = new model.Payment();
        payment.setId(GlobalValues.generateId());
        payment.setAccount(bank);
        payment.setAmount(amount);
        payment.setDate(new Date(System.currentTimeMillis()));
        payment.setTime(new Time(System.currentTimeMillis()));
        DBManager.save(model.Payment.class, payment);

        return payment;
    }

    public List<model.Payment> all(){
        return DBManager.listAll(model.Payment.class);
    }

    public List<model.Payment> filterByDates(Date start, Date end){
        return DBManager.query(model.Payment.class, "select p from Payment p where p.date >= ?1 and p.date <= ?2", start, end);
    }
}
