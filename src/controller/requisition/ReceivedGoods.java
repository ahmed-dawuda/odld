package controller.requisition;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;

import java.sql.Date;
import java.util.Arrays;

public class ReceivedGoods {
    @FXML private TableView<GoodReceived> requestTable;
    @FXML private TableColumn<GoodReceived, Date> dateCol;
    @FXML private TableColumn<GoodReceived, Zone> zoneCol;
    @FXML private TableColumn<GoodReceived, Transfer> statusCol;
    @FXML private TableColumn<GoodReceived, Transfer> dateRequestedCol;
    @FXML private TableColumn<GoodReceived, Worker> workerCol;


    @FXML private TableView<TransferStatus> goodsTable;
    @FXML private TableColumn<TransferStatus, Product> productCol;
    @FXML private TableColumn<TransferStatus, Double> goodCol;
    @FXML private TableColumn<TransferStatus, Double> badCol;
    @FXML private TableColumn<TransferStatus, Double> totalCol;

    private utility.Transfer transferUtil = new utility.Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, zoneCol, statusCol, dateRequestedCol, workerCol), Arrays.asList("date", "zone", "originator", "originator", "worker"));
        ViewUtility.initColumns(Arrays.asList(productCol, goodCol, badCol, totalCol), Arrays.asList("product", "good", "bad", "total"));

        workerCol.setCellFactory(c -> new TableCell<GoodReceived, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFirstname() + " " + item.getSurname());
            }
        });

        dateCol.setCellFactory(c -> new TableCell<GoodReceived, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });
        zoneCol.setCellFactory(c -> new TableCell<GoodReceived, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        statusCol.setCellFactory(c -> new TableCell<GoodReceived, Transfer>(){
            @Override
            protected void updateItem(Transfer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : transferUtil.getStatus(item));
            }
        });

        dateRequestedCol.setCellFactory(c -> new TableCell<GoodReceived, Transfer>(){
            @Override
            protected void updateItem(Transfer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item.getDate()));
            }
        });

        productCol.setCellFactory(c -> new TableCell<TransferStatus, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        goodCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        badCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        totalCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(newValue.getTransferStatuses());
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.receivedGoods()));
        goodsTable.setItems(FXCollections.observableArrayList());
        requestTable.getSelectionModel().selectFirst();
    }
}
