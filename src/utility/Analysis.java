package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Cell;
import model.Product;

import java.sql.Date;
import java.util.List;

public class Analysis {


    public AnalysisItem createAnalysisItem(model.Analysis item, Product product, double preQty, double curQty, Date stockBalAsAt){

        AnalysisItem analysisItem = new AnalysisItem();
        analysisItem.setId(GlobalValues.generateId());
        analysisItem.setProduct(product);
        analysisItem.setCurrentStock(curQty);
        analysisItem.setUnitPrice(product.getPrice());
        analysisItem.setSupplyOn(preQty);
        analysisItem.setQuantity(preQty - curQty);
        analysisItem.setAmount(analysisItem.getQuantity() * analysisItem.getUnitPrice());
        analysisItem.setStockBalAsAt(stockBalAsAt);
        analysisItem.setTotalStockAsAt(new Date(System.currentTimeMillis()));
        analysisItem.setAnalysis(item);

        if(item != null) {
            DBManager.save(AnalysisItem.class, analysisItem);
        }
        return analysisItem;
    }

    public model.Analysis createAnalysis(Cell cell){
        model.Analysis analysis = new model.Analysis();
        analysis.setId(GlobalValues.generateId());
        analysis.setCell(cell);
        analysis.setDate(new Date(System.currentTimeMillis()));
        DBManager.save(model.Analysis.class, analysis);
        return analysis;
    }

    public AnalysisItem cloneAnalysisItem(AnalysisItem item){
        AnalysisItem analysisItem = new AnalysisItem();
        analysisItem.setAnalysis(null);
        analysisItem.setAmount(item.getAmount());
        analysisItem.setTotalStockAsAt(item.getTotalStockAsAt());
        analysisItem.setStockBalAsAt(item.getStockBalAsAt());
        analysisItem.setCurrentStock(item.getCurrentStock());
        analysisItem.setId(GlobalValues.generateId());
        analysisItem.setQuantity(item.getQuantity());
        analysisItem.setProduct(item.getProduct());
        analysisItem.setSupplyOn(item.getSupplyOn());
        analysisItem.setUnitPrice(item.getUnitPrice());
        return analysisItem;
    }

    public List<model.Analysis> analyses(){
//        List<Cell> cells = DBManager.query(Cell.class, "SELECT c FROM Cell c where c.worker = ?1", GlobalValues.getWorker());
        return DBManager.query(model.Analysis.class, "SELECT a FROM Analysis a ORDER BY a.date DESC ");
//        return DBManager.listAll(model.Analysis.class);
    }

    public List<model.Analysis> analysesByCell(Cell cell) {
        return DBManager.query(model.Analysis.class, "SELECT a FROM Analysis a WHERE a.cell = ?1 ORDER BY a.date DESC ", cell);
    }

    public List<model.Analysis> getAnalysisByZone(model.Zone zone) {
        List<Cell> cells = DBManager.query(Cell.class, "SELECT c FROM Cell c where c.zone = ?1", zone);
        return DBManager.query(model.Analysis.class, "SELECT a FROM Analysis a WHERE a.cell IN ?1 ORDER BY a.date DESC", cells);
    }
}
