package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Product;
import model.Requisition;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Transfer {

    public model.Transfer create(){
        Requisition requisition = new Requisition();
        requisition.setId(GlobalValues.generateId());
        requisition.setDate(new Date(System.currentTimeMillis()));
        requisition.setTime(new Time(System.currentTimeMillis()));
        DBManager.save(Requisition.class, requisition);
        return requisition;
    }

    public List<Requisition> allRequisitions(){
        return DBManager.listAll(Requisition.class);
    }

    public List<Good> getGoodsByRequisition(Requisition requisition){
        return DBManager.query(Good.class, "select g from Good g where g.transfer = ?1", requisition);
    }

    public String type(Number id){
        model.Transfer transfer = DBManager.find(model.Transfer.class, id);
        return getStatus(transfer);
    }

    public List<model.Requisition> all() {
        return DBManager.query(model.Requisition.class, "select r from Requisition r order by r.date desc");
    }

    public List<IssueCommand> issueCommands(){
        return DBManager.query(IssueCommand.class, "Select i from IssueCommand i order by i.date desc");
    }

    public Good createGood(model.Transfer transfer, Product product, double quantity, double instock, double quantityRequested){
        Good good = new Good();
        good.setId(GlobalValues.generateId());
        good.setProduct(product);
        good.setInstock(instock);
        good.setQuantity(quantity);
        good.setQuantityRequested(quantityRequested);
        if (transfer != null) {
            good.setTransfer(transfer);
            DBManager.save(Good.class, good);
        }
        return good;
    }

    public IssueCommand createIssueCommand(Requisition requisition){
        IssueCommand issueCommand = new IssueCommand();
        issueCommand.setId(GlobalValues.generateId());
        issueCommand.setZone(requisition.getZone());
        issueCommand.setWorker(requisition.getWorker());
        issueCommand.setDate(new Date(System.currentTimeMillis()));
        issueCommand.setTime(new Time(System.currentTimeMillis()));
        issueCommand.setOriginator(requisition);
        DBManager.save(IssueCommand.class, issueCommand);
        return issueCommand;
    }

    public String getStatus(model.Transfer originator){
        String status = "Pending";

        long integer = DBManager.queryForSingleResult(Long.class, "select count(g.id) from GoodReceived g where g.originator = ?1", originator);
        if (integer > 0) {
            return "Completed";
        }

        long integer2 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from Issue g where g.originator = ?1", originator);
        if (integer2 > 0) {
            return "Issued";
        }

        long integer1 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from IssueCommand g where g.originator = ?1", originator);
        if (integer1 > 0) {
            return "Approved";
        }

        return status;
    }

    public List<Issue> issues(){
        return DBManager.query(Issue.class, "Select i from Issue i order by i.date desc");
    }

    public List<GoodReceived> receivedGoods(){
        return DBManager.query(GoodReceived.class, "select g from GoodReceived g order by g.date desc");
    }
}
