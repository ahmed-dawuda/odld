package controller;


import controller.popups.EditMinStock;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Location;
import model.Product;
import model.Worker;
import model.Zone;
import utility.*;
import utility.Cell;
import utility.Warehouse;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Inventory {

    @FXML private TableView<model.Inventory> inventoryTable;
    @FXML private TableColumn<model.Inventory, Product> productCol;
    @FXML private TableColumn<model.Inventory, Double> quantityCol;
    @FXML private TableColumn<model.Inventory, Product> priceCol;
    @FXML private TableColumn<model.Inventory, Double> amountCol;
    @FXML private TableColumn<model.Inventory, Double> minStock;
    @FXML private TableColumn<model.Inventory, Number> editMin;

    @FXML private TableView<Location> locationsTable;
    @FXML private TableColumn<Location, String> nameCol;
    @FXML private TableColumn<Location, Number> typeCol;
    @FXML private TableColumn<Location, model.Worker> officerCol;

    @FXML private ChoiceBox<String> show;
    @FXML private ChoiceBox<model.Zone> zone;


    private utility.Inventory inventoryUtil = new utility.Inventory();
    private utility.Location locationUtil = new utility.Location();
    private utility.Zone zoneUtil = new utility.Zone();
    private utility.Warehouse warehouseUtil = new Warehouse();
    private Cell cellUtil = new Cell();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, priceCol, minStock, editMin), Arrays.asList("product", "quantity", "product", "minStock", "id"));
        ViewUtility.initColumns(Arrays.asList(nameCol, typeCol, officerCol), Arrays.asList("name", "id", "worker"));

        zone.setDisable(true);
        show.setItems(FXCollections.observableArrayList(Arrays.asList("All Locations", "Warehouse Only", "Zones Only", "Cells Only")));
        zone.setConverter(new StringConverter<Zone>() {
            @Override
            public String toString(Zone object) {
                return object.getName();
            }

            @Override
            public Zone fromString(String string) {
                return null;
            }
        });



        /**
         * COLUMNS FOR LOCATIONS TABLE
         */

        editMin.setCellFactory(c -> new TableCell<model.Inventory, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/editMinStock", "Edit the minimum stock for this inventory");
                    EditMinStock controller = modal.getController();
                    controller.init(inventoryTable.getItems().get(getTableRow().getIndex()), inventoryTable.getItems());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        minStock.setCellFactory(c -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        typeCol.setCellFactory(c -> new TableCell<Location, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : locationUtil.type(item));
            }
        });

        officerCol.setCellFactory(c -> new TableCell<Location, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
//                setText(item == null || empty ? null : item.getFirstname() + " " + item.getOthername() + " " + item.getSurname());
                if (empty) {
                    setText(null);
                } else {
                    setText(item == null ? "Not Yet Assigned" : item.getFirstname() + " " + item.getOthername() + " "+ item.getSurname());
                }
            }
        });

        /**
         * COLUMNS FOR INVENTORY TABLE
         */
        amountCol.setCellValueFactory(cellData -> {
            double amount = cellData.getValue().getQuantity() * cellData.getValue().getProduct().getPrice();
            return new SimpleObjectProperty<>(amount);
        });

        productCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item ==  null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                if (item != null && !empty) {
                    int index = getTableRow().getIndex();
                    model.Inventory inventory = inventoryTable.getItems().get(index);
                    setStyle(item < inventory.getMinStock() ? "-fx-font-weight: bold; -fx-text-fill: #e65100" : "-fx-font-weight: normal");
                }
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item.getPrice()));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        /**
         * LISTENERS AND INITIALIZERS
         */
        inventoryTable.setItems(FXCollections.observableArrayList());

        locationsTable.setItems(FXCollections.observableArrayList());

        zone.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) return;
            locationsTable.getItems().clear();
            System.out.println(newValue.getCells());
            locationsTable.getItems().addAll(newValue.getCells());
        });

        locationsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                inventoryTable.getItems().clear();
                return;
            }
            inventoryTable.getItems().clear();
            inventoryTable.getItems().addAll(newValue.getInventory());
        });

        zone.setItems(FXCollections.observableArrayList());

        show.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equalsIgnoreCase("All Locations")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(locationUtil.all());
            } else if(newValue.equalsIgnoreCase("Warehouse Only")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(warehouseUtil.all());
            } else if (newValue.equalsIgnoreCase("Zones Only")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(zoneUtil.all());
            } else if (newValue.equalsIgnoreCase("Cells Only")) {
                zone.setDisable(false);
                locationsTable.getItems().clear();
                zone.getItems().clear();
                zone.getItems().addAll(zoneUtil.all());
                zone.getSelectionModel().selectFirst();
            }
            locationsTable.getSelectionModel().selectFirst();
        });

        show.getSelectionModel().selectFirst();
        locationsTable.getSelectionModel().selectFirst();
    }
}
