package controller.popups;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.Location;
import model.Worker;

import java.util.*;

public class AddWorker {

    @FXML private TextField firstname;
    @FXML private TextField surname;
    @FXML private TextField othername;
    @FXML private TextField phone;
    @FXML private TextField password;

    @FXML private ChoiceBox<Assignment> assignment;
    @FXML private ChoiceBox<Location> wLocation;

    private utility.Worker workerUtil = new utility.Worker();
    private utility.Location locationUtil = new utility.Location();
    private List<Worker> workersTable;

    private Worker worker = null;

    private List<Assignment> list = new ArrayList<>();

    public void initialize(){
        assignment.setConverter(new StringConverter<Assignment>() {
            @Override
            public String toString(Assignment object) {
                return object.getName();
            }

            @Override
            public Assignment fromString(String string) {
                return null;
            }
        });

        wLocation.setConverter(new StringConverter<Location>() {
            @Override
            public String toString(Location object) {
                return object.getName();
            }

            @Override
            public Location fromString(String string) {
                return null;
            }
        });

        wLocation.setItems(FXCollections.observableArrayList());

        assignment.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            wLocation.getItems().clear();
            wLocation.getItems().addAll(locationUtil.officesByLevel(newValue.level));
            wLocation.getSelectionModel().selectFirst();
        });

//        List<Assignment> list = new ArrayList<>();
        list.add(new Assignment(0, "Manager"));
        list.add(new Assignment(1, "Warehouse Officer"));
        list.add(new Assignment(2, "Zonal Officer"));

        assignment.setItems(FXCollections.observableArrayList(list));
        assignment.getSelectionModel().selectFirst();
    }

    public void init(List<Worker> workersTable, Worker... workers){
        this.workersTable = workersTable;
        if (workers.length > 0) {
            this.worker = workers[0];
            firstname.setText(worker.getFirstname());
            surname.setText(worker.getSurname());
            othername.setText(worker.getOthername().equalsIgnoreCase("-") ? "" : worker.getOthername());
            password.setText(worker.getPassword());
            phone.setText(worker.getPhone());

            assignment.getSelectionModel().select(list.get(this.worker.getLevel()));
            wLocation.getSelectionModel().select(this.worker.getPrimaryLocation());
        }
    }

    public void save(){
        if (firstname.getText().trim().isEmpty()) {
            ViewUtility.alert("first name is required", "You must provide the worker's firstname");
            return;
        } else if (surname.getText().trim().isEmpty()) {
            ViewUtility.alert("surname is required", "You must provide the worker's surname");
            return;
        } else if (phone.getText().trim().isEmpty()) {
            ViewUtility.alert("Phone is required", "You must provide the worker's firstname");
            return;
        } else if (password.getText().trim().isEmpty()) {
            ViewUtility.alert("Password not yet generated", "Please click to generate a password for this worker");
            return;
        } else if (assignment.getSelectionModel().getSelectedItem() == null) {
            ViewUtility.alert("Assignment is required", "Please you need to provide the role for this worker");
            return;
        } else if (wLocation.getSelectionModel().getSelectedItem() == null) {
            ViewUtility.alert("Select Location to be assigned", "Please you need to provide the Location for this worker");
            return;
        }

        if (this.worker != null) {
            DBManager.begin();
            worker.setPhone(phone.getText().trim());
            worker.setOthername(othername.getText().trim());
            worker.setSurname(surname.getText().trim());
            worker.setPassword(password.getText().trim());
            worker.setFirstname(firstname.getText().trim());
            worker.setPrimaryLocation(wLocation.getSelectionModel().getSelectedItem());
            DBManager.commit();
            ViewUtility.closeWindow(password);
        } else {
            Worker worker = workerUtil.create(
                    firstname.getText().trim(),
                    surname.getText().trim(),
                    phone.getText().trim(),
                    password.getText().trim(),
                    othername.getText().trim(),
                    wLocation.getSelectionModel().getSelectedItem());

            this.workersTable.add(worker);
            ViewUtility.closeWindow(password);
        }
    }

    public void generatePassword(){
        String passwd = UUID.randomUUID().toString().substring(0,7);
        password.setText(passwd);
    }

    protected class Assignment{
        private int level;
        private String name;

        public Assignment(int level, String name) {
            this.level = level;
            this.name = name;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
