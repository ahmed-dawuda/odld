package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;

import java.util.List;

public class Category {
    public List<model.Category> all() {
        return DBManager.listAll(model.Category.class);
    }

    public model.Category createCategory(String name, String abbr){
        model.Category category = new model.Category();
        category.setId(GlobalValues.generateId());
        category.setAbbreviation(abbr);
        category.setName(name);
        DBManager.save(model.Category.class, category);
        return  category;
    }
}
