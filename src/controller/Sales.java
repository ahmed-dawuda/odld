package controller;

import controller.popups.DetailSale;
import controller.popups.NewSale;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Invoice;
import model.Location;
import model.Worker;
import model.Zone;
import utility.*;
import utility.Cell;
import utility.Warehouse;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Sales {

    @FXML private TableView<Invoice> salesTable;
    @FXML private TableColumn<Invoice, Date> dateCol;
    @FXML private TableColumn<Invoice, Time> timeCol;
    @FXML private TableColumn<Invoice, String> customerCol;
    @FXML private TableColumn<Invoice, Double> amountCol;
    @FXML private TableColumn<Invoice, Double> discountCol;
    @FXML private TableColumn<Invoice, Double> netCol;
    @FXML private TableColumn<Invoice, Number> viewCol;
    @FXML private TableColumn<Invoice, String> saleTypeCol;

    @FXML private TableView<Location> locationsTable;
    @FXML private TableColumn<Location, String> nameCol;
    @FXML private TableColumn<Location, Number> typeCol;
    @FXML private TableColumn<Location, Worker> officerCol;

    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;

    @FXML private TextField totalSales;
    @FXML private TextField totalDiscount;
    @FXML private TextField netSales;

    @FXML private ChoiceBox<String> show;
    @FXML private ChoiceBox<Zone> zone;

    private utility.Invoice invoiceUtil = new utility.Invoice();
    private utility.Location locationUtil = new utility.Location();
    private utility.Zone zoneUtil = new utility.Zone();
    private Warehouse warehouseUtil = new Warehouse();
    private Cell cellUtil = new Cell();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, customerCol, amountCol, discountCol, netCol, viewCol, saleTypeCol), Arrays.asList("date", "time", "customer", "total", "discount", "payable", "id", "type"));
        ViewUtility.initColumns(Arrays.asList(nameCol, typeCol, officerCol), Arrays.asList("name", "id", "worker"));

        show.setItems(FXCollections.observableArrayList(Arrays.asList("All Locations", "Warehouse Only", "Zone Only", "Cells Only")));
        zone.setDisable(true);
        zone.setConverter(new StringConverter<Zone>() {
            @Override
            public String toString(Zone object) {
                return object.getName();
            }

            @Override
            public Zone fromString(String string) {
                return null;
            }
        });
        /**
         * CELL FACTORIES
         */
        typeCol.setCellFactory(c -> new TableCell<Location, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : locationUtil.type(item));
            }
        });

        officerCol.setCellFactory(c -> new TableCell<Location, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
//                setText(item == null || empty ? null : item.getFirstname() + " " + item.getOthername() + " " + item.getSurname());
                if (empty) {
                    setText(null);
                } else {
                    setText(item == null ? "Not Yet Assigned" : item.getFirstname() + " " + item.getOthername() + " "+ item.getSurname());
                }
            }
        });

        viewCol.setCellFactory(cell ->  new TableCell<Invoice, Number>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Detail");
                link.setOnAction(event -> {
                    int index = getTableRow().getIndex();
                    FXMLLoader modal = ViewUtility.modal("popups/detailSale", "Invoice", true);
                    DetailSale detailSaleController = modal.getController();
                    detailSaleController.init(salesTable.getItems().get(index));
                });
                link.setTextFill(Color.GREEN);
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        saleTypeCol.setCellFactory(c -> new TableCell<Invoice, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    setText(item.equalsIgnoreCase("cash") ? "Cash Sale" : "Credit Sale");
                }
            }
        });

        dateCol.setCellFactory(cell -> new TableCell<Invoice, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(cell -> new TableCell<Invoice, Time>(){
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

//        DecimalFormat df = new DecimalFormat("#0.00");
        amountCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        discountCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        netCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        locationsTable.setItems(FXCollections.observableArrayList());

        locationsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            salesTable.getItems().clear();
//            System.out.println(newValue);
            salesTable.getItems().addAll(newValue.getInvoices());
        });

        show.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equalsIgnoreCase("All Locations")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
//                System.out.println(locationUtil.all());
                locationsTable.getItems().addAll(locationUtil.all());
            } else if (newValue.equalsIgnoreCase("Warehouse Only")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(warehouseUtil.all());
            } else if (newValue.equalsIgnoreCase("Zone Only")) {
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(zoneUtil.all());
            } else if (newValue.equalsIgnoreCase("Cells Only")) {
                locationsTable.getItems().clear();
                zone.getItems().clear();
                zone.getItems().addAll(zoneUtil.all());
                zone.getSelectionModel().selectFirst();
                zone.setDisable(false);
            }
            locationsTable.getSelectionModel().selectFirst();
        });

        show.getSelectionModel().selectFirst();
        locationsTable.getSelectionModel().selectFirst();
        salesTable.itemsProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                totalSales.setText("0");
                totalDiscount.setText(GlobalValues.getCurrencyInstance().format("0"));
                netSales.setText(GlobalValues.getCurrencyInstance().format("0"));
            }
            else {
                double totalsales = 0, totaldiscount = 0, netsales = 0;
                for (model.Invoice invoice : newValue) {
                    totalsales += invoice.getTotal();
                    totaldiscount += invoice.getDiscount();
                    netsales += invoice.getPayable();
                }
                totalSales.setText(GlobalValues.getCurrencyInstance().format(totalsales));
                totalDiscount.setText(GlobalValues.getCurrencyInstance().format(totaldiscount));
                netSales.setText(GlobalValues.getCurrencyInstance().format(netsales));
            }
        });


        salesTable.setItems(FXCollections.observableArrayList());

        salesTable.getItems().addListener((ListChangeListener<Invoice>) c -> {
            ObservableList<? extends model.Invoice> list = c.getList();
            double totalsales = 0, totaldiscount = 0, netsales = 0;
            for (Invoice invoice : list) {
                totalsales += invoice.getTotal();
                totaldiscount += invoice.getDiscount();
                netsales += invoice.getPayable();
            }
            totalSales.setText(GlobalValues.getCurrencyInstance().format(totalsales));
            totalDiscount.setText(GlobalValues.getCurrencyInstance().format(totaldiscount));
            netSales.setText(GlobalValues.getCurrencyInstance().format(netsales));
        });


    }

    public void filter(){
        LocalDate start = startDate.getValue() == null ? LocalDate.of(2015, 01, 01) : startDate.getValue();
        LocalDate end = endDate.getValue() == null ? LocalDate.now().plusDays(1) : endDate.getValue().plusDays(1);
        List<model.Invoice> sales = invoiceUtil.filterByDates(Date.valueOf(start), Date.valueOf(end));
        salesTable.getItems().clear();
        salesTable.getItems().addAll(sales);
//        System.out.println(sales);
    }
}
