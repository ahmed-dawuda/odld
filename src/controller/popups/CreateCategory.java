package controller.popups;


import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.Category;

import java.util.List;

public class CreateCategory {
    @FXML private TextField name;
    @FXML private TextField abbreviation;

    private Category toEdit = null;
    private utility.Category categoryUtil = new utility.Category();
    private List<Category> categoryTable;

    public void create() {
        if (name.getText().isEmpty()) {
            ViewUtility.alert("Category Name is required", "You have not provided the name of this category.");
        } else if (abbreviation.getText().isEmpty()) {
            ViewUtility.alert("Category Abbreviation is required", "Please provide the abbreviation for this category");
        } else if (toEdit == null) {
            Category category = categoryUtil.createCategory(name.getText().trim(), abbreviation.getText().trim());
            this.categoryTable.add(category);
            ViewUtility.alert("Success", "Category created successfully");
            ViewUtility.closeWindow(name);
        } else {
            categoryTable.remove(toEdit);
            DBManager.begin();
            toEdit.setName(name.getText().trim());
            toEdit.setAbbreviation(abbreviation.getText().trim());
            DBManager.commit();
            categoryTable.add(toEdit);
            ViewUtility.alert("Success", "You have successfully edited this category");
            ViewUtility.closeWindow(name);
        }
    }

    public void init(List<Category> categories, Category... cats){
        this.categoryTable = categories;
        if (cats.length > 0){
            this.toEdit = cats[0];
            name.setText(toEdit.getName());
            abbreviation.setText(toEdit.getAbbreviation());
        }
    }
}
