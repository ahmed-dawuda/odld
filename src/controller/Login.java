package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import mStuff.ViewUtility;

import java.io.IOException;

public class Login {

    @FXML private TextField username;
    @FXML private PasswordField password;

    public void initialize(){

    }

    public void login() throws IOException {
//        if (username.getText().trim().isEmpty() || password.getText().trim().isEmpty()) {
//            ViewUtility.alert("No username or password", "Please make sure all fields are provided");
//        } else {
            Stage window = (Stage) username.getScene().getWindow();
            EventHandler<WindowEvent> onCloseRequest = window.getOnCloseRequest();
            window.setOnCloseRequest(null);
            this.quit();
            ViewUtility.newWindow("master", "Desert Lion Group | Director", onCloseRequest);
//        }
    }

    public void quit(){
        ViewUtility.closeWindow(username);
    }
}
