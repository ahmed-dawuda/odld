package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Location;
import model.Product;
import model.Worker;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Inventory {

    public List<model.Inventory> all(){
        return DBManager.listAll(model.Inventory.class);
    }

    /**
     *
     * @return products that are in the inventory. similar to select product from inventories
     */
    public List<model.Product> products(){
        List<model.Inventory> inventories = all();
        List<Product> products = new ArrayList<>();
        for (model.Inventory inventory : inventories) {
            products.add(inventory.getProduct());
        }
        return products;
    }

    public model.Inventory getInventoryByProduct(Product product){
        String query = "select i from Inventory i where i.product = ?1";
        List<model.Inventory> inventories = DBManager.query(model.Inventory.class, query, product);
        model.Inventory inventory = null;
        if (inventories.size() > 0) {
            inventory = inventories.get(0);
        }
        return inventory;
    }


    public void initializeInventory(Product product){

        List<Location> locations = DBManager.listAll(Location.class);

        for (Location location : locations) {
            model.Inventory inventory = new model.Inventory();
            inventory.setId(GlobalValues.generateId());
            inventory.setPrevQty(0);
            inventory.setQuantity(0);
            inventory.setProduct(product);
            inventory.setLastUpdated(new Date(System.currentTimeMillis()));
            inventory.setLocation(location);
            DBManager.save(model.Inventory.class, inventory);
            DBManager.begin();
            location.getInventory().add(inventory);
            DBManager.commit();
        }
    }

    public List<model.Inventory> initInventoryForLocation(Location location){
        List<Product> products = DBManager.listAll(Product.class);
        List<model.Inventory> inventories = new ArrayList<>();
        for (Product product : products) {
            model.Inventory inventory = new model.Inventory();
            inventory.setId(GlobalValues.generateId());
            inventory.setLocation(location);
            inventory.setProduct(product);
            DBManager.save(model.Inventory.class, inventory);
            inventories.add(inventory);
        }
        return inventories;
    }
}
