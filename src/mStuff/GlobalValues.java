package mStuff;


import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

public class GlobalValues {
    public static Number generateId(){
        return System.currentTimeMillis();
    }

    public static DecimalFormat getDefaultDecimalFormat() {
        return new DecimalFormat("#0.00");
    }

    public static DecimalFormat getCurrencyInstance() {
        return new DecimalFormat("¢###,###,##0.00");
    }

    public static DecimalFormat getCommaSeparatedInstance() {
        return new DecimalFormat("###,###,##0.00");
    }

    public static DateFormat getDateInstance() {
        return new SimpleDateFormat("E, dd MMM yyyy");
    }

    public static DateFormat getTimeInstance() {
        return new SimpleDateFormat("hh:mm aa");
    }

    public static final String ORANGE = "#e65100";

    public static final List<String> months = Arrays.asList("JANUARY", "FEBRUARY", "MARCH", "ARPIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");

    public static DecimalFormat getZeroPadded3(){
        return new DecimalFormat("#000");
    }
}
