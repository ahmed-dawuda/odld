package utility;


import mStuff.DBManager;

import java.util.ArrayList;
import java.util.List;

public class Location {

    utility.Warehouse warehouseUtil = new Warehouse();
    Zone zoneUtil = new Zone();
    Cell cellUtil = new Cell();

    public String type(Number id) {
        String type = "";
        if (warehouseUtil.find(id) != null) {
            type = "Warehouse";
        } else if (zoneUtil.find(id) != null) {
            type = "Zone";
        } else if (cellUtil.find(id) != null) {
            type = "Cell";
        }
        return type;
    }

    public List<model.Location> all(){
        return DBManager.listAll(model.Location.class);
    }

    public List officesByLevel(int level) {
        List result = new ArrayList();
        switch (level) {
            case 1:
                result = DBManager.listAll(model.Warehouse.class);
                break;
            case 2:
                result = DBManager.listAll(model.Zone.class);
                break;
            default:

        }
        return result;
    }
}
