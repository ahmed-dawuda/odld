package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.util.List;

public class Account {

    public List<model.Account> all(){
        return DBManager.listAll(model.Account.class);
    }

    public void update(model.Account data, model.Payment payment){
        data.getPayments().add(payment);
    }

    public model.Account create(String bank, String branch, String name, String number) {

        model.Account account = new model.Account();
        account.setId(GlobalValues.generateId());
        account.setBank(bank);
        account.setBranch(branch);
        account.setName(name);
        account.setNumber(number);
        DBManager.save(model.Account.class, account);
        return account;
    }

    public model.Account updateAccount(model.Account account, String bank, String branch, String name, String number){
        DBManager.begin();
        account.setBank(bank);
        account.setBranch(branch);
        account.setName(name);
        account.setNumber(number);
        DBManager.commit();
        return account;
    }
}
