package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "goodsreceived")
@Inheritance
public class GoodReceived extends Transfer {

    @OneToMany(mappedBy = "goodsreceived", fetch = FetchType.LAZY)
    List<TransferStatus> transferStatuses = new ArrayList<>();

    public List<TransferStatus> getTransferStatuses() {
        return transferStatuses;
    }

    public void setTransferStatuses(List<TransferStatus> transferStatuses) {
        this.transferStatuses = transferStatuses;
    }
}
