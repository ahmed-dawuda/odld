package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import model.Good;
import model.Product;
import model.Transfer;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;


public class RequisitionManagement {

    @FXML private VBox content;

    public void initialize() throws IOException {
        loadContent("requisitions");
    }

    public void filter(ActionEvent actionEvent) {

    }

    public void changeRequisition(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();
        loadContent(text);
    }

    public void loadContent(String filename) throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + filename + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
