package controller;

import controller.popups.Logout;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import mStuff.ViewUtility;

import java.io.IOException;

public class Master {


    public VBox content;
    public Button requisitions;
    public Button sales;
    public Button payments;
    public Button inventory;
    public Label header;

    @FXML private Button cashtrack;
    private SimpleBooleanProperty logout = new SimpleBooleanProperty(false);

    public void initialize() throws IOException {
        header.setText("Desert Lion Group > DASHBOARD");
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/inventory.fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
//        ((HBox) cashtrack.getParent()).getChildren().remove(cashtrack);
    }

    public void changeContent(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();
        logout.addListener((observable, oldValue, newValue) -> {
            try {
                ViewUtility.newWindow("login", "Login", ((Stage) inventory.getScene().getWindow()).getOnCloseRequest());
                ViewUtility.closeWindow(header);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        if (text.equalsIgnoreCase("logout")) {
            FXMLLoader modal = ViewUtility.modal("popups/alerts/logout", "Confirm logout");
            Logout controller = modal.getController();
            controller.init(logout);
        } else {
            header.setText("Desert Lion Group > " + text.toUpperCase());
            Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/" + text + ".fxml"));
            content.getChildren().clear();
            VBox.setVgrow(load, Priority.ALWAYS);
            content.getChildren().add(load);
        }
    }
}
