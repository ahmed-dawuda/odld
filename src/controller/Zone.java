package controller;


import controller.popups.CreateWarehouse;
import controller.popups.CreateZone;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Cell;
import model.Warehouse;
import model.Worker;

import java.util.Arrays;

public class Zone {

    @FXML private TableView<model.Zone> zonesTable;
    @FXML private TableColumn<model.Zone, String> nameCol;
    @FXML private TableColumn<model.Zone, model.Worker> workerCol;
    @FXML private TableColumn<model.Zone, Number> removeCol;
    @FXML private TableColumn<model.Zone, Number> editCol;

    @FXML private TableView<Cell> cellsTable;
    @FXML private TableColumn<Cell, String> cellCol;

    private utility.Zone zoneUtil = new utility.Zone();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, workerCol, removeCol, editCol), Arrays.asList("name", "worker", "id", "id"));
        ViewUtility.initTableColumn(cellCol, "name");

        cellsTable.setItems(FXCollections.observableArrayList());

        workerCol.setCellFactory(c -> new TableCell<model.Zone, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                    return;
                }
                setText(item == null ? "Not yet assigned" : item.getFirstname() + " " + item.getOthername() + " " + item.getSurname());
            }
        });

        removeCol.setCellFactory(c -> new TableCell<model.Zone, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                link.setOnAction(e -> {
                    zonesTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        editCol.setCellFactory(c -> new TableCell<model.Zone, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/createzone", "Edit zone");
                    CreateZone createZoneController = modal.getController();
                    createZoneController.init(zonesTable.getItems(), zonesTable.getItems().get(getTableRow().getIndex()));
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        zonesTable.setItems(FXCollections.observableArrayList(zoneUtil.all()));

        zonesTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            cellsTable.getItems().clear();
            cellsTable.getItems().addAll(newValue.getCells());
//            cellsTable.setItems(FXCollections.observableArrayList(newValue.getCells()));
//            for (model.Cell cell : newValue.getCells()) {
//                cellsTable.getItems().add(cell);
//            }
        });
    }

    public void createZone(){
        FXMLLoader modal = ViewUtility.modal("popups/createzone", "Create New Zone");
        CreateZone createZoneController = modal.getController();
        createZoneController.init(zonesTable.getItems());
    }
}
