package model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "inventories")
public class Inventory {

    @Id
    @Column(name = "id")
    private Number id;

    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "prevQty")
    private double prevQty;

    @ManyToOne
    @JoinColumn(name = "location")
    private Location location;

    @Column(name = "lastUpdated")
    private Date lastUpdated;

    @Column(name = "minstock")
    private double minStock = 50;

    public double getMinStock() {
        return minStock;
    }

    public void setMinStock(double minStock) {
        this.minStock = minStock;
    }

    public double getPrevQty() {
        return prevQty;
    }

    public void setPrevQty(double prevQty) {
        this.prevQty = prevQty;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
