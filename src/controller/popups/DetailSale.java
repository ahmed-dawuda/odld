package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mStuff.ViewUtility;
import model.Invoice;
import model.Product;
import model.Sale;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class DetailSale {
    @FXML private TableView<Sale> saleItemsTable;
    @FXML private TableColumn<Sale, Product> productCol;
    @FXML private TableColumn<Sale, Double> quantityCol;
    @FXML private TableColumn<Sale, Double> priceCol;
    @FXML private TableColumn<Sale, Double> amountCol;

    @FXML private TextField customername;
    @FXML private TextField totalTF;
    @FXML private TextField payableTF;
    @FXML private TextField discountTF;

    private utility.Sale saleUtil = new utility.Sale();

    public void init(Invoice invoice){
        List<Sale> sales = invoice.getSales();
        if (sales.size() == 0) {
            sales = saleUtil.getSalesByInvoice(invoice);
        }
        saleItemsTable.setItems(FXCollections.observableArrayList(sales));
        customername.setText(invoice.getCustomer());
        DecimalFormat df = new DecimalFormat("#0.00");
        totalTF.setText(df.format(invoice.getTotal()));
        payableTF.setText(df.format(invoice.getPayable()));
        discountTF.setText(df.format(invoice.getDiscount()));
    }

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, priceCol, amountCol), Arrays.asList("product", "quantity", "price", "amount"));

        productCol.setCellFactory(cell -> new TableCell<Sale, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        DecimalFormat df = new DecimalFormat("#0.00");

        quantityCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<Sale, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : df.format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        saleItemsTable.setItems(FXCollections.observableArrayList());
    }
}
