package controller.expenses;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Expense;
import model.Location;
import model.Worker;
import model.Zone;

import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;

public class Expenses {

    @FXML private TableView<Expense> expenseTable;
    @FXML private TableColumn<Expense, Date> dateCol;
    @FXML private TableColumn<Expense, Time> timeCol;
    @FXML private TableColumn<Expense, String> purposeCol;
    @FXML private TableColumn<Expense, Double> amountCol;
//    @FXML private TextField total;

    @FXML private TableView<Worker> workerTable;
    @FXML private TableColumn<Worker, Location> zoneCol;
    @FXML private TableColumn<Worker, Number> officerCol;

    private utility.Expense expenseUtil = new utility.Expense();
    private utility.Worker workerUtility = new utility.Worker();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(zoneCol, officerCol), Arrays.asList("primaryLocation", "id"));

        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, purposeCol, amountCol), Arrays.asList("date", "time", "comment", "amount"));

        dateCol.setCellFactory(c -> new TableCell<Expense, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(c -> new TableCell<Expense, Time>(){
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });


        zoneCol.setCellFactory(c -> new TableCell<Worker, Location>(){
            @Override
            protected void updateItem(Location item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        officerCol.setCellFactory(c -> new TableCell<Worker, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty){
                    setText(null);
                } else {
                    setText(workerTable.getItems().get(getTableRow().getIndex()).toString());
                }
            }
        });

        amountCol.setCellFactory(c -> new TableCell<Expense, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        workerTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            expenseTable.getItems().clear();
            expenseTable.getItems().addAll(newValue.getExpenses());
        });
        workerTable.setItems(FXCollections.observableArrayList(workerUtility.all()));
        expenseTable.setItems(FXCollections.observableArrayList());
        workerTable.getSelectionModel().selectFirst();
    }

    public void filter(ActionEvent event) {

    }
}
