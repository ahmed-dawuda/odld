package controller.requisition;


import controller.popups.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import org.eclipse.persistence.annotations.Array;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class Requisition {

    @FXML private TableView<model.Transfer> requestTable;
    @FXML private TableColumn<model.Transfer, Date> dateCol;
    @FXML private TableColumn<model.Transfer, Zone> zoneCol;
    @FXML private TableColumn<model.Transfer, Worker> workerCol;
    @FXML private TableColumn<model.Transfer, Number> statusCol;


    @FXML private TableView<Good> detailTable;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Double> instockCol;
    @FXML private TableColumn<Good, Product> productCol;

    private utility.Transfer transferUtil = new utility.Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, zoneCol, statusCol, workerCol), Arrays.asList("date", "zone", "id", "worker"));
        ViewUtility.initColumns(Arrays.asList(quantityCol, instockCol, productCol), Arrays.asList("quantity", "instock", "product"));

        workerCol.setCellFactory(c -> new TableCell<Transfer, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFirstname() + " " + item.getSurname());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        instockCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
//                System.out.println(item);
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        detailTable.setItems(FXCollections.observableArrayList());

        dateCol.setCellFactory(c -> new TableCell<model.Transfer, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        zoneCol.setCellFactory(c -> new TableCell<model.Transfer, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        statusCol.setCellFactory(c -> new TableCell<model.Transfer, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : transferUtil.type(item));
            }
        });

        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            detailTable.getItems().clear();
            detailTable.getItems().addAll(newValue.getGoods());
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.all()));
        requestTable.getSelectionModel().selectFirst();
    }

    public void approve(){
        if (!transferUtil.getStatus(requestTable.getSelectionModel().getSelectedItem()).equalsIgnoreCase("Pending")) {
            ViewUtility.alert("Requisition Already Approved", "You can not approve an already approved requisition");
            return;
        }
        FXMLLoader modal = ViewUtility.modal("popups/issueCommand", "Approve requisition");
        controller.popups.IssueCommand controller = modal.getController();
        controller.init(((model.Requisition) requestTable.getSelectionModel().getSelectedItem()), requestTable.getItems());
    }
}
