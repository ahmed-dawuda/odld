package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.ViewUtility;
import model.Worker;
import model.Zone;

import java.util.List;

public class CreateZone {
    @FXML private TextField name;
//    @FXML private ChoiceBox<Worker> worker;

    private List<Zone> zonesTable;
    private Zone zone = null;

    private utility.Zone zoneUtil = new utility.Zone();
    private utility.Worker workerUtil = new utility.Worker();

    public void initialize(){
//        worker.setConverter(new StringConverter<Worker>() {
//            @Override
//            public String toString(Worker object) {
//                return object.getFirstname() + " " + object.getOthername() + " " + object.getSurname();
//            }
//
//            @Override
//            public Worker fromString(String string) {
//                return null;
//            }
//        });
//
//        worker.setItems(FXCollections.observableArrayList(workerUtil.getWorkersByLevel(2)));
////        worker.getSelectionModel().selectFirst();
    }

    public void init(List<Zone> zones, Zone... zone) {
        zonesTable = zones;
        if (zone.length > 0) {
            this.zone = zone[0];
            this.name.setText(this.zone.getName());
//            this.worker.getSelectionModel().select(this.zone.getWorker());
        }
    }

    public void create(){
        if (name.getText().trim().isEmpty()) {
            ViewUtility.alert("Name is required", "You must provide the name of this zone");
            return;
        }

        if (this.zone != null) {
            zoneUtil.update(this.zone, name.getText().trim());
            quit();
        } else {
            Zone zone = zoneUtil.create(name.getText().trim());
            zonesTable.add(zone);
            quit();
        }
    }

    private void quit(){
        ViewUtility.closeWindow(name);
    }
}
