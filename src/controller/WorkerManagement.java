package controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class WorkerManagement {

    @FXML private VBox content;

    public void initialize() throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/workers.fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }


    public void changeContent(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + text + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
