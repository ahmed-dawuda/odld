package controller;


import controller.popups.CreateWarehouse;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Worker;

import java.util.Arrays;

public class Warehouse {

    @FXML private TableView<model.Warehouse> warehouseTable;
    @FXML private TableColumn<model.Warehouse, String> nameCol;
    @FXML private TableColumn<model.Warehouse, model.Worker> workerCol;
    @FXML private TableColumn<model.Warehouse, Number> removeCol;
    @FXML private TableColumn<model.Warehouse, Number> editCol;

    private utility.Warehouse warehouseUtil = new utility.Warehouse();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, workerCol, removeCol, editCol), Arrays.asList("name", "worker", "id", "id"));
        workerCol.setCellFactory(cell -> new TableCell<model.Warehouse, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                    return;
                }
                setText(item == null ? "Not yet assigned" : item.getFirstname() + " " + item.getOthername() + " " + item.getSurname());
            }
        });

        removeCol.setCellFactory(c -> new TableCell<model.Warehouse, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setOnAction(e -> {
                    warehouseTable.getItems().remove(getTableRow().getIndex());
                });
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        editCol.setCellFactory(c -> new TableCell<model.Warehouse, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/createwarehouse", "Edit warehouse");
                    CreateWarehouse createWarehouseController = modal.getController();
                    createWarehouseController.init(warehouseTable.getItems(), warehouseTable.getItems().get(getTableRow().getIndex()));
                });

                link.setTextFill(Color.GREEN);
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        warehouseTable.setItems(FXCollections.observableArrayList(warehouseUtil.all()));
    }

    public void create(){
        FXMLLoader modal = ViewUtility.modal("popups/createwarehouse", "Create new warehouse");
        CreateWarehouse createWarehouseController = modal.getController();
        createWarehouseController.init(warehouseTable.getItems());
    }
}
