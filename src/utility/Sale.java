package utility;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Invoice;
import model.Product;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class Sale {

    public model.Sale create(double amount, model.Invoice invoice, Product product, double quantity){

        model.Sale sale = new model.Sale();
        sale.setId(GlobalValues.generateId());
        sale.setAmount(amount);
        sale.setInvoice(invoice);
        sale.setPrice(product.getPrice());
        sale.setProduct(product);
        sale.setQuantity(quantity);
        DBManager.save(model.Sale.class, sale);

        return sale;
    }

    public List<model.Sale> getSalesByInvoice(Invoice invoice){
        return DBManager.query(model.Sale.class, "select s from Sale s where s.invoice = ?1", invoice);
    }

    public ObservableList<PieChart.Data> weeklySales(int month, int year){



        String date = month < 10 ? year +"-0"+ month + "-" : year +"-"+ month + "-";
        System.out.println(date);

        ObservableList<PieChart.Data> salesList = FXCollections.observableArrayList();
        int week = 1;

        for(int day = 1; day < 29; day+=7){
            String query = "select sum(i.payable) from Invoice i where i.date >= ?1 and i.date <= ?2";
            String date1 = date + day;
            String date2 = date + (day + 6);
            System.out.println(date1);
            List sales = DBManager.query(query,date1 ,date2 );
            salesList.add(new PieChart.Data("Week "+week, sales.get(0) == null ? 0 : (Double) sales.get(0)));
            week++;
        }
        return salesList;
    }

    public ObservableList<PieChart.Data> chartBySales(int month, int year){
        int week = 1;
        ObservableList<PieChart.Data> salesList = FXCollections.observableArrayList();
        for (int day = 1; day < 29; day += 7) {
            String query = "Select sum(i.payable) from Invoice i where i.date >= ?1 and i.date <= ?2";
            List sales = DBManager.query(query, Date.valueOf(LocalDate.of(year, month, day)), Date.valueOf(LocalDate.of(year, month, day+6)));
//            System.out.println(sales);
            salesList.add(new PieChart.Data("Week "+week, sales.get(0) == null ? 0 : (Double) sales.get(0)));

//            System.out.println("week "+ week +": " + sales.get(0));
            week++;
        }

        return salesList;
    }

}
