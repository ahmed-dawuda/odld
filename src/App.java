import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import mStuff.DBManager;
import mStuff.GlobalValues;
import model.Account;
import model.Inventory;
import model.Product;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        initialize();

        primaryStage.getIcons().add(new Image("/view/icons/lion_icon.png"));
        primaryStage.setTitle("Desert Lion Group");

        Parent root = FXMLLoader.load(getClass().getResource("/view/template/content/login.fxml"));
        primaryStage.setScene(new Scene(root));

        primaryStage.setOnCloseRequest(event -> close());
        primaryStage.show();
//        initTables();
    }

    private void initTables() {
        DBManager.listAll(Account.class);
    }

    private void initialize() {
//        DBManager.createDbDirectory("database");
//        DBManager.init("SQLite");
        if (DBManager.createDbDirectory("C:/database") && DBManager.init("SQLite"))
            initTables();
        else {
            System.err.println("Could not initialize database");
            System.exit(-23);
        }

//        data();
//        products();
    }

    private void close() {
        DBManager.close();
    }

    public void data(){
        Account account = new Account();
        account.setId(GlobalValues.generateId());
        account.setBank("SGSS Bank");
        account.setBranch("Asokore Mampong");
        account.setName("Dorakt");
        account.setNumber("353956000939645");
        DBManager.save(Account.class, account);

        Account account1 = new Account();
        account1.setId(GlobalValues.generateId());
        account1.setBank("Ecobank");
        account1.setBranch("Ashaiman");
        account1.setName("Dorakt");
        account1.setNumber("043956800939698");
        DBManager.save(Account.class, account1);

        Account account2 = new Account();
        account2.setId(GlobalValues.generateId());
        account2.setBank("Barclays");
        account2.setBranch("Sunyani");
        account2.setName("Money lending");
        account2.setNumber("778402975219396");
        DBManager.save(Account.class, account2);
    }

    public void products(){
        Product product = new Product();
        product.setId(GlobalValues.generateId());
        product.setName("Akati Global");
        product.setPrice(34.25);
        product.setUpdated_at(new Timestamp(System.currentTimeMillis()));
        DBManager.save(Product.class, product);

        Product product1 = new Product();
        product1.setId(GlobalValues.generateId());
        product1.setName("Trimangol Insecticide");
        product1.setPrice(50);
        product1.setUpdated_at(new Timestamp(System.currentTimeMillis()));
        DBManager.save(Product.class, product1);

        Product product2 = new Product();
        product2.setId(GlobalValues.generateId());
        product2.setName("Knapsack sprayer");
        product2.setPrice(34.25);
        product2.setUpdated_at(new Timestamp(System.currentTimeMillis()));
        DBManager.save(Product.class, product2);

        Inventory inventory = new Inventory();
        inventory.setId(GlobalValues.generateId());
        inventory.setProduct(product);
        inventory.setQuantity(3000);
        DBManager.save(Inventory.class, inventory);

        Inventory inventory1 = new Inventory();
        inventory1.setId(GlobalValues.generateId());
        inventory1.setProduct(product1);
        inventory1.setQuantity(439);
        DBManager.save(Inventory.class, inventory1);

        Inventory inventory2 = new Inventory();
        inventory2.setId(GlobalValues.generateId());
        inventory2.setProduct(product2);
        inventory2.setQuantity(576);
        DBManager.save(Inventory.class, inventory2);
    }

}
