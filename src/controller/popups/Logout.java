package controller.popups;


import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import mStuff.ViewUtility;

public class Logout {
    @FXML private Button yes;
    private SimpleBooleanProperty response;

    public void initialize(){

    }


    public void logoutAction(ActionEvent actionEvent) {
        String text = ((Button) actionEvent.getSource()).getText().toLowerCase();
        if (text.trim().equalsIgnoreCase("yes")) {
            ViewUtility.closeWindow(yes);
            response.set(true);
        } else {
            ViewUtility.closeWindow(yes);
        }
    }

    public void init(SimpleBooleanProperty response){
        this.response = response;
    }
}
