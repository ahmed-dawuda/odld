package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import mStuff.ViewUtility;

import java.io.IOException;


public class ExpensesManagement {
    @FXML
    private VBox content;

    public void initialize() throws IOException {
        loadGraph("expenses");
    }

    public void changeContent(ActionEvent event) throws IOException {
        String graph = ((Button) event.getSource()).getId().toLowerCase();
        loadGraph(graph);
    }

    public void loadGraph(String filename) throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + filename + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
