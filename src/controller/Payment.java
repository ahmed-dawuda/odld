package controller;


import controller.popups.NewPayment;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.util.Callback;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Account;
import model.Location;
import model.Worker;
import utility.*;
import utility.Warehouse;
import utility.Zone;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Payment {

    /**
     * datepicker and text fields on the view
     */
    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;
    @FXML private TextField total;

    /**
     * table and table columns definition
     */
    @FXML private TableView<model.Payment> paymentTable;
    @FXML private TableColumn<model.Payment, Date> dateCol;
    @FXML private TableColumn<model.Payment, Time> timeCol;
    @FXML private TableColumn<model.Payment, Account> bankCol;
    @FXML private TableColumn<model.Payment, Double> amountCol;
    @FXML private TableColumn<model.Payment, Account> branchCol;
    @FXML private TableColumn<model.Payment, Account> accountCol;

    @FXML private TableView<Location> locationsTable;
    @FXML private TableColumn<Location, String> nameCol;
    @FXML private TableColumn<Location, Number> typeCol;
    @FXML private TableColumn<Location, model.Worker> officerCol;

    @FXML private ChoiceBox<String> show;
    @FXML private ChoiceBox<model.Zone> zone;

    /**
     * properties
     */
//    private List<model.Payment> payments = new ArrayList<>();
    private utility.Payment paymentUtil = new utility.Payment();
    private utility.Location locationUtil = new utility.Location();
    private Zone zoneUtil = new Zone();
    private Warehouse warehouseUtil = new Warehouse();

    /**
     * initializing the view page
     */
    public void initialize(){
        /**
         * initializing tables
         */
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, bankCol, amountCol, branchCol, accountCol), Arrays.asList("date", "time", "account", "amount", "account", "account"));
        ViewUtility.initColumns(Arrays.asList(nameCol, typeCol, officerCol), Arrays.asList("name", "id" , "worker"));
        zone.setDisable(true);

        zone.setConverter(new StringConverter<model.Zone>() {
            @Override
            public String toString(model.Zone object) {
                return object.getName();
            }

            @Override
            public model.Zone fromString(String string) {
                return null;
            }
        });

        show.setItems(FXCollections.observableArrayList(Arrays.asList("All Locations", "Warehouse Only", "Zone Only", "Cells Only")));

        typeCol.setCellFactory(c -> new TableCell<Location, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : locationUtil.type(item));
            }
        });

        officerCol.setCellFactory(c -> new TableCell<Location, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(item == null ? "Not Yet Assigned" : item.getFirstname() + " " + item.getOthername() + " "+ item.getSurname());
                }
//                setText(item == null || empty ? null : item.getFirstname() + " " + item.getOthername() + " " + item.getSurname());
            }
        });

        locationsTable.setItems(FXCollections.observableArrayList());
        /**
         * listeners
         */

        total.setAlignment(Pos.BASELINE_RIGHT);

//        DecimalFormat df = new DecimalFormat("#0.00");

        amountCol.setCellFactory(cell -> new TableCell<model.Payment, Double>() {
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        timeCol.setCellFactory(cell -> new TableCell<model.Payment, Time>() {
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

        dateCol.setCellFactory(cell -> new TableCell<model.Payment, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        bankCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getBank());
            }
        });

        branchCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getBranch());
            }
        });

        accountCol.setCellFactory(cell -> new TableCell<model.Payment, Account>() {
            @Override
            protected void updateItem(Account item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getNumber());
            }
        });

        paymentTable.itemsProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                total.setText("");
            }
            else {
                double totalAmount = 0;
                for (model.Payment payment : newValue) {
                    totalAmount += payment.getAmount();
                }
                total.setText(GlobalValues.getCurrencyInstance().format(totalAmount));
            }
        });

        /**
         * fetching the payments
         */
//        payments = DBManager.listAll(model.Payment.class);

        paymentTable.setItems(FXCollections.observableArrayList());


        paymentTable.getItems().addListener((ListChangeListener<model.Payment>) c -> {
            ObservableList<? extends model.Payment> list = c.getList();
            double totalAmount = 0;
            for (model.Payment payment : list) {
                totalAmount += payment.getAmount();
            }
            total.setText(GlobalValues.getCurrencyInstance().format(totalAmount));
        });

        show.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equalsIgnoreCase("All Locations")) {
                zone.getSelectionModel().clearSelection();
                zone.setDisable(true);
                paymentTable.getItems().clear();
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(locationUtil.all());
            } else if(newValue.equalsIgnoreCase("Warehouse Only")) {
                zone.getSelectionModel().clearSelection();
                paymentTable.getItems().clear();
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(warehouseUtil.all());
            } else if (newValue.equalsIgnoreCase("Zone Only")) {
                zone.getSelectionModel().clearSelection();
                paymentTable.getItems().clear();
                zone.setDisable(true);
                locationsTable.getItems().clear();
                locationsTable.getItems().addAll(zoneUtil.all());
            } else if (newValue.equalsIgnoreCase("Cells Only")) {
                zone.setDisable(false);
                paymentTable.getItems().clear();
                zone.getItems().clear();
                zone.getItems().addAll(zoneUtil.all());
                zone.getSelectionModel().selectFirst();
                locationsTable.getItems().clear();
            }
        });

        show.getSelectionModel().selectFirst();

        locationsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            paymentTable.getItems().clear();
            paymentTable.getItems().addAll(newValue.getWorker().getPayments());
        });

        locationsTable.getSelectionModel().selectFirst();
    }

    public void makePayment(){
        FXMLLoader modal = ViewUtility.modal("popups/makepayment", "Make new Payment", true);
        NewPayment newPaymentController = modal.getController();
        newPaymentController.init(paymentTable.getItems());
    }

    public void filter(){
        LocalDate start = startDate.getValue() == null ? LocalDate.of(2015, 01, 01) : startDate.getValue();
        LocalDate end = endDate.getValue() == null ? LocalDate.now().plusDays(1) : endDate.getValue().plusDays(1);
        List<model.Payment> payments = paymentUtil.filterByDates(Date.valueOf(start), Date.valueOf(end));
        paymentTable.getItems().clear();
        paymentTable.getItems().addAll(payments);
    }

}
