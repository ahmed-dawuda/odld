package controller.requisition;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;

import java.sql.Date;
import java.util.Arrays;

public class ApprovedRequisition {

    @FXML private TableView<Transfer> requestTable;
    @FXML private TableColumn<Transfer, Date> dateCol;
    @FXML private TableColumn<model.Transfer, Zone> zoneCol;
    @FXML private TableColumn<model.Transfer, Worker> workerCol;
    @FXML private TableColumn<model.Transfer, Number> statusCol;

    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> newQuantity;
    @FXML private TableColumn<Good, Double> quantityRequested;
    @FXML private TableColumn<Good, Double> instock;

    private utility.Transfer transferUtil = new utility.Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, zoneCol, statusCol, workerCol), Arrays.asList("date", "zone", "id", "worker"));
        ViewUtility.initColumns(Arrays.asList(newQuantity, quantityRequested, productCol, instock), Arrays.asList("quantity", "quantityRequested", "product", "instock"));

        workerCol.setCellFactory(c -> new TableCell<Transfer, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFirstname() + " "+ item.getSurname());
            }
        });
        newQuantity.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        instock.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });


        quantityRequested.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        dateCol.setCellFactory(c -> new TableCell<Transfer, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        zoneCol.setCellFactory(c -> new TableCell<Transfer, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        statusCol.setCellFactory(c -> new TableCell<Transfer, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : "Approved");
            }
        });

        goodsTable.setItems(FXCollections.observableArrayList());
        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(newValue.getGoods());
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.issueCommands()));
        requestTable.getSelectionModel().selectFirst();
    }
}
