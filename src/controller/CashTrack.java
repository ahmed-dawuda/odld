package controller;


import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Credit;
import model.Debit;
import model.Worker;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CashTrack {
    public TableView<CashMatters> cashTable;
    public TableColumn<CashMatters, Worker> workerCol;
    public TableColumn<CashMatters, Double> cashInCol;
    public TableColumn<CashMatters, Double> cashOutCol;
    public TableColumn<CashMatters, Double> diffCol;
    public TableColumn<CashMatters, Date> lastPaymentCol;
    public TableColumn<CashMatters, Date> lastSaleDate;

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(workerCol, cashInCol, cashOutCol, diffCol, lastPaymentCol, lastSaleDate), Arrays.asList("worker", "cashIn", "CashOut", "difference", "lastPaymentDate", "lastSaleDate"));
        workerCol.setCellFactory(c -> new TableCell<CashMatters, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.toString());
//                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        cashInCol.setCellFactory(c -> new TableCell<CashMatters, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        cashOutCol.setCellFactory(c -> new TableCell<CashMatters, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        diffCol.setCellFactory(c -> new TableCell<CashMatters, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        lastPaymentCol.setCellFactory(c -> new TableCell<CashMatters, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(item == null ? "No Payment Made Yet" : GlobalValues.getDateInstance().format(item));
                }
//                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        lastSaleDate.setCellFactory(c -> new TableCell<CashMatters, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(item == null ? "No Sale Made Yet" : GlobalValues.getDateInstance().format(item));
                }
//                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        List<Worker> workers = DBManager.listAll(Worker.class);
        List<CashMatters> cashMatters = new ArrayList<>();
        for (Worker worker : workers) {
            cashMatters.add(new CashMatters(worker));
        }

        cashTable.setItems(FXCollections.observableArrayList(cashMatters));

    }

    protected class CashMatters{
        private Worker worker;
        private double cashIn = 0;
        private double cashOut = 0;
        private double difference;
        private Date lastPaymentDate;
        private Date lastSaleDate;

        public CashMatters(Worker worker){
            super();
            this.worker = worker;
            if (DBManager.queryForSingleResult(Long.class, "Select count(c) from Credit c where c.worker = ?1", worker) > 0) {
                this.cashIn = DBManager.queryForSingleResult(Double.class, "Select sum(c.amount) from Credit c where c.worker = ?1", worker);
            }

            if (DBManager.queryForSingleResult(Long.class, "select count(d) from Debit d where d.worker = ?1", worker) > 0) {
                this.cashOut = DBManager.queryForSingleResult(Double.class, "Select sum(d.amount) from Debit d where d.worker = ?1", worker);
            }

            this.difference = cashIn - cashOut;
            this.lastPaymentDate = DBManager.queryForSingleResult(Date.class, "Select max(p.date) from Payment p where p.worker = ?1", worker);
            this.lastSaleDate = DBManager.queryForSingleResult(Date.class, "Select max(c.date) from Credit c where c.worker = ?1", worker);
        }

        public CashMatters(){
            super();
        }

        public Date getLastSaleDate() {
            return lastSaleDate;
        }

        public void setLastSaleDate(Date lastSaleDate) {
            this.lastSaleDate = lastSaleDate;
        }

        public Worker getWorker() {
            return worker;
        }

        public void setWorker(Worker worker) {
            this.worker = worker;
        }

        public double getCashIn() {
            return cashIn;
        }

        public void setCashIn(double cashIn) {
            this.cashIn = cashIn;
        }

        public double getCashOut() {
            return cashOut;
        }

        public void setCashOut(double cashOut) {
            this.cashOut = cashOut;
        }

        public double getDifference() {
            return difference;
        }

        public void setDifference(double difference) {
            this.difference = difference;
        }

        public Date getLastPaymentDate() {
            return lastPaymentDate;
        }

        public void setLastPaymentDate(Date lastPaymentDate) {
            this.lastPaymentDate = lastPaymentDate;
        }

        @Override
        public String toString() {
            return "CashMatters{" +
                    "worker=" + worker +
                    ", cashIn=" + cashIn +
                    ", cashOut=" + cashOut +
                    ", difference=" + difference +
                    ", lastPaymentDate=" + lastPaymentDate +
                    '}';
        }
    }
}
