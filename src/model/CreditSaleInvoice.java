package model;


import javax.persistence.*;

@Entity
@Table(name = "creditSaleInvoice")
@Inheritance
public class CreditSaleInvoice extends Invoice {
    @Column(name = "amountPaid")
    private double amountPaid;

    @Column(name = "balance")
    private double balance;

    @JoinColumn(name = "debtor")
    @ManyToOne
    private Customer debtor;

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Customer getDebtor() {
        return debtor;
    }

    public void setDebtor(Customer debtor) {
        this.debtor = debtor;
    }
}
