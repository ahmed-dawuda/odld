package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;

public class Product {

    public List<model.Product> all(){
        return DBManager.listAll(model.Product.class);
    }

    public model.Product find(Number id) {
        return DBManager.find(model.Product.class, id);
    }

    public model.Product create(String code, model.Category category, String name, Double price, double wholesale){
        model.Product product = new model.Product();
        product.setId(GlobalValues.generateId());
        product.setName(name);
        product.setPrice(price);
        product.setCategory(category);
        product.setCode(code);
        product.setWholesale(wholesale);
        product.setUpdated_at(new Timestamp(System.currentTimeMillis()));
        DBManager.save(model.Product.class, product);
        return product;
    }

    public String newCodeNumber () {
        Long aLong = DBManager.queryForSingleResult(Long.class, "Select count(p) from Product p");
        return  new DecimalFormat("#000").format(aLong + 1);
    }

    public model.Product getProductByCode(String code){
        model.Product product = null;
        List<model.Product> products = DBManager.query(model.Product.class, "Select p from Product p where p.code = ?1", code);
        if (products.size() > 0) {
            product = products.get(0);
        }
        return product;
    }
}
