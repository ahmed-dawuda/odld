package controller;


import controller.popups.CreateCategory;
import controller.popups.NewProduct;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Category;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Arrays;

public class Product {

    @FXML private TableView<model.Product> productsTable;
    @FXML private TableColumn<model.Product, String> nameCol;
    @FXML private TableColumn<model.Product, Double> priceCol;
    @FXML private TableColumn<model.Product, Number> removeCol;
    @FXML private TableColumn<model.Product, Number> editCol;
    @FXML private TableColumn<model.Product, Timestamp> updated;
    @FXML private TableColumn<model.Product, String> codeCol;
    @FXML private TableColumn<model.Product, Double> wholesaleCol;

    @FXML private TableView<Category> categoryTable;
    @FXML private TableColumn<Category, String> categoryNameCol;
    @FXML private TableColumn<Category, String> categoryAbbrCol;
    @FXML private TableColumn<Category, Number> categoryEditCol;

    private utility.Product productUtil = new utility.Product();
    private utility.Category categoryUtil = new utility.Category();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, priceCol, removeCol, editCol, updated, codeCol, wholesaleCol), Arrays.asList("name", "price", "id", "id", "updated_at", "code", "wholesale"));
        ViewUtility.initColumns(Arrays.asList(categoryAbbrCol, categoryNameCol, categoryEditCol), Arrays.asList("abbreviation", "name", "id"));

        categoryEditCol.setCellFactory(c -> new TableCell<Category, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/addCategory", "Add New Category", true);
                    CreateCategory controller = modal.getController();
                    controller.init(categoryTable.getItems(), categoryTable.getItems().get(getTableRow().getIndex()));
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

//        codeCol.setCellFactory(c -> new TableCell<model.Product, Number>(){
//            @Override
//            protected void updateItem(Number item, boolean empty) {
//                super.updateItem(item, empty);
//                if (item == null || empty) {
//                    setText(null);
//                } else {
//                    model.Product product = productsTable.getItems().get(getTableRow().getIndex());
//                    setText(product.getCategory().getAbbreviation() + GlobalValues.getZeroPadded3().format(item));
//                }
//            }
//        });

        wholesaleCol.setCellFactory(c -> new TableCell<model.Product, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

       updated.setCellFactory(c -> new TableCell<model.Product, Timestamp>(){
           @Override
           protected void updateItem(Timestamp item, boolean empty) {
               super.updateItem(item, empty);
               setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
           }
       });

        priceCol.setCellFactory(c -> new TableCell<model.Product, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        removeCol.setCellFactory(c -> new TableCell<model.Product, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setOnAction(e -> {
                    productsTable.getItems().remove(getTableRow().getIndex());
                });
                link.setTextFill(Color.valueOf("#e65100"));
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);

            }
        });

        editCol.setCellFactory(c -> new TableCell<model.Product, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/addproduct", "Edit product");
                    NewProduct newProductController = modal.getController();
                    newProductController.init(productsTable.getItems().get(getTableRow().getIndex()));
                });
                link.setTextFill(Color.GREEN);
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        productsTable.setItems(FXCollections.observableArrayList());

        categoryTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            productsTable.getItems().clear();
            productsTable.getItems().addAll(newValue.getProducts());
        });

        categoryTable.setItems(FXCollections.observableArrayList(categoryUtil.all()));
        categoryTable.getSelectionModel().selectFirst();
    }

    public void addProduct(){
        FXMLLoader modal = ViewUtility.modal("popups/addproduct", "Add new product");
    }

    public void addCategory(){
        FXMLLoader modal = ViewUtility.modal("popups/addCategory", "Add New Category", true);
        CreateCategory controller = modal.getController();
        controller.init(categoryTable.getItems());
    }
}
