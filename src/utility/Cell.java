package utility;

import mStuff.DBManager;

import java.util.List;

public class Cell {
    public model.Cell find(Number id){
        return DBManager.find(model.Cell.class, id);
    }

    public List<model.Cell> all(){
        return DBManager.listAll(model.Cell.class);
    }
}

