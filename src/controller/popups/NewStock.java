package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Product;
import model.Stock;
import utility.Inventory;

import java.sql.Date;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class NewStock {
    @FXML private ChoiceBox<Product> productCB;
    @FXML private TextField quantityTF;
    @FXML private TextField inStock;
    @FXML private TextField totalStock;

    @FXML private TableView<Stock> goodsTable;
    @FXML private TableColumn<Stock, Product> productCol;
    @FXML private TableColumn<Stock, Double> quantityCol;
    @FXML private TableColumn<Stock, Double> inStockCol;
    @FXML private TableColumn<Stock, Number> controlsCol;


    private utility.Product productUtil = new utility.Product();
    private Inventory inventoryUtil = new Inventory();

    public void initialize(){
        DecimalFormat decimalFormat = new DecimalFormat("#0.00");

        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, inStockCol, controlsCol), Arrays.asList("product", "quantity", "instock", "id"));

        productCol.setCellFactory(c -> new TableCell<Stock, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Stock, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : decimalFormat.format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        inStockCol.setCellFactory(c -> new TableCell<Stock, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : decimalFormat.format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        controlsCol.setCellFactory(c -> new TableCell<Stock, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setStyle("-fx-text-fill: #e65100");
                link.setOnAction(event -> {
                    goodsTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        goodsTable.setItems(FXCollections.observableArrayList());

        productCB.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        productCB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            model.Inventory inventory = inventoryUtil.getInventoryByProduct(newValue);
            inStock.setText(decimalFormat.format(inventory.getQuantity()));
            String qty = quantityTF.getText().isEmpty() ? "0" : quantityTF.getText();
            totalStock.setText(decimalFormat.format(inventory.getQuantity() + Double.parseDouble(qty)));
        });

        quantityTF.textProperty().addListener((observable, oldValue, newValue) -> {
            double qty;
            if (newValue.isEmpty()){
                oldValue = "0";
            }
            try{
                qty = Double.parseDouble(newValue);
                totalStock.setText(decimalFormat.format(qty + Double.parseDouble(inStock.getText())));
            }catch (Exception ex){
                quantityTF.setText(oldValue);
            }
        });

        productCB.setItems(FXCollections.observableArrayList(productUtil.all()));
        productCB.getSelectionModel().selectFirst();
    }

    public void finish(){
        List<Stock> stocks = goodsTable.getItems();
        for (Stock stock : stocks) {
            model.Inventory inventory = inventoryUtil.getInventoryByProduct(stock.getProduct());
            DBManager.begin();
            inventory.setQuantity(inventory.getQuantity() + stock.getQuantity());
            DBManager.commit();
            DBManager.save(Stock.class, stock);
        }
        goodsTable.getItems().clear();
        quantityTF.clear();
        productCB.getSelectionModel().selectFirst();
        ViewUtility.closeWindow(productCB);
    }

    public void addItem(){
        if (quantityTF.getText().isEmpty() || quantityTF.getText().equalsIgnoreCase("0")) {
            ViewUtility.alert("Quantity not provided", "You must specify the quantity of " + productCB.getSelectionModel().getSelectedItem().getName());
            return;
        }

        List<Stock> stocks = goodsTable.getItems();
        for (Stock stock : stocks) {
            if (stock.getProduct().getName().equalsIgnoreCase(productCB.getSelectionModel().getSelectedItem().getName())) {
                ViewUtility.alert("Product already added", productCB.getSelectionModel().getSelectedItem().getName()+" has been added, please remove it before adding again");
                return;
            }
        }

        Stock stock = new Stock();
        stock.setDate(new Date(System.currentTimeMillis()));
        stock.setTime(new Time(System.currentTimeMillis()));
        stock.setId(GlobalValues.generateId());
        stock.setInstock(Double.parseDouble(inStock.getText()));
        stock.setQuantity(Double.parseDouble(quantityTF.getText()));
        stock.setProduct(productCB.getSelectionModel().getSelectedItem());

        goodsTable.getItems().add(stock);
    }
}
