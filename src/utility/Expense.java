package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.ExpenseType;

import java.util.List;

public class Expense {
    public List<ExpenseType> allExpenseTypes(){
        return DBManager.listAll(ExpenseType.class);
    }

    public List<model.Expense> allExpenses(){
        return DBManager.listAll(model.Expense.class);
    }

    public ExpenseType createExpenseType(String name, double limit){
        ExpenseType expenseType = new ExpenseType();
        expenseType.setId(GlobalValues.generateId());
        expenseType.setCeiling(limit);
        expenseType.setName(name);
        DBManager.save(ExpenseType.class, expenseType);
        return expenseType;
    }
}
