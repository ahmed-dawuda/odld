package controller.popups;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.ExpenseType;
import utility.Expense;

import java.util.List;

public class NewExpenseType {
    @FXML private TextField name;
    @FXML private TextField limit;

    private List<ExpenseType> typesTable;
    private ExpenseType type = null;

    private Expense expenseUtil = new Expense();

    public void initialize(){
        limit.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double d = Double.parseDouble(newValue);
            } catch (Exception e){
                limit.setText(oldValue);
            }
        });
    }

    public void create(ActionEvent event) {
        if (name.getText().isEmpty()){
            ViewUtility.alert("Name is required", "You must provide the name of this expense");
        } else if (limit.getText().isEmpty()){
            ViewUtility.alert("Limit is required", "This will set a control on the maximun they can spend on this expense type");
        } else if (type != null) {
            typesTable.remove(type);
            DBManager.begin();
            type.setName(name.getText().trim());
            type.setCeiling(Double.parseDouble(limit.getText()));
            DBManager.commit();
            typesTable.add(type);
            ViewUtility.alert("Success", "Expense Type edited successfully");
            ViewUtility.closeWindow(name);
        } else {
            ExpenseType expenseType = expenseUtil.createExpenseType(name.getText().trim(), Double.parseDouble(limit.getText().trim()));
            typesTable.add(expenseType);
            ViewUtility.alert("Success", "Expense Type created successfully");
            ViewUtility.closeWindow(name);
        }
    }

    public void init(List<ExpenseType> types, ExpenseType... type){
        this.typesTable = types;
        if (type.length > 0) {
            this.type = type[0];
            name.setText(this.type.getName());
            limit.setText(GlobalValues.getDefaultDecimalFormat().format(this.type.getCeiling()));
        }
    }
}
