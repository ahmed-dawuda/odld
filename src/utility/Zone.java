package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Cell;
import model.Worker;

import java.util.List;

public class Zone {
    private Inventory inventoryUtil = new Inventory();

    public model.Zone find(Number id) {
        return DBManager.find(model.Zone.class, id);
    }

    public List<model.Zone> all() {
        return DBManager.listAll(model.Zone.class);
    }

    public model.Zone create(String name){
        model.Zone zone = new model.Zone();
        zone.setId(GlobalValues.generateId());
        zone.setName(name);
//        zone.setWorker(worker);
        DBManager.save(model.Zone.class, zone);
        List<model.Inventory> inventories = inventoryUtil.initInventoryForLocation(zone);
        DBManager.begin();
        zone.setInventory(inventories);
        DBManager.commit();
        return zone;
    }

    public model.Zone update(model.Zone zone, String name) {
        DBManager.begin();
        zone.setName(name);
//        zone.setWorker(worker);
//        worker.setPrimaryLocation(zone);
        DBManager.commit();
        return  zone;
    }

    public List<Cell> getCells(model.Zone zone){
        return DBManager.query(Cell.class, "SELECT c FROM Cell c WHERE c.zone = ?1", zone);
    }
}
