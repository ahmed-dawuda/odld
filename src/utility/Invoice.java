package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Invoice {

    public List<model.Invoice> all(){
        return DBManager.listAll(model.Invoice.class);
    }

    public model.Invoice create(String customername, double discount, double payable, double total){
        model.Invoice invoice = new model.Invoice();
        invoice.setId(GlobalValues.generateId());
        invoice.setDate(new Date(System.currentTimeMillis()));
        invoice.setTime(new Time(System.currentTimeMillis()));
        invoice.setCustomer(customername.trim());
        invoice.setDiscount(discount);
        invoice.setPayable(payable);
        invoice.setTotal(total);
        /**
         * worker, location
         */
        DBManager.save(model.Invoice.class, invoice);
        return invoice;
    }

    public List<model.Invoice> filterByDates(Date start, Date end){
        return DBManager.query(model.Invoice.class, "select i from Invoice i where i.date >= ?1 and i.date <= ?2", start, end);
    }
}
