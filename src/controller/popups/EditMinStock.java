package controller.popups;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Inventory;

import java.util.ArrayList;
import java.util.List;

public class EditMinStock {
    @FXML private TextField minStock;

    private Inventory inventory = null;
    private List<Inventory> inventories = new ArrayList<>();

    public void initialize(){
        minStock.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double stock = Double.parseDouble(newValue);
            } catch (Exception ex) {
                minStock.setText(oldValue);
            }
        });
    }

    public void save(){
        if (minStock.getText().trim().isEmpty()) {
            ViewUtility.alert("Minimum stock is required", "You must set a minimum stock for this inventory product");
            return;
        }
        this.inventories.remove(this.inventory);
        DBManager.begin();
        this.inventory.setMinStock(Double.parseDouble(minStock.getText().trim()));
        DBManager.commit();
        this.inventories.add(this.inventory);
        ViewUtility.closeWindow(minStock);
    }

    public void init(Inventory inventory, List<Inventory> inventories) {
        this.inventory = inventory;
        this.inventories = inventories;
        minStock.setText(GlobalValues.getDefaultDecimalFormat().format(inventory.getMinStock()));
    }
}
