package controller.expenses;


import controller.popups.NewExpenseType;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.ExpenseType;
import utility.Expense;

import java.util.Arrays;

public class ExpenseTypes {

    @FXML private TableView<ExpenseType> typesTable;
    @FXML private TableColumn<ExpenseType, String> nameCol;
    @FXML private TableColumn<ExpenseType, Double> limitCol;
    @FXML private TableColumn<ExpenseType, Number> editCol;
    @FXML private TableColumn<ExpenseType, Number> removeCol;

    private Expense expenseUtil = new Expense();


    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, limitCol, editCol, removeCol), Arrays.asList("name", "ceiling", "id", "id"));
        limitCol.setCellFactory(c -> new TableCell<ExpenseType, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        editCol.setCellFactory(c -> new TableCell<ExpenseType, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    ExpenseType expenseType = typesTable.getItems().get(getTableRow().getIndex());
                    FXMLLoader modal = ViewUtility.modal("popups/newExpenseType", "New Type", true);
                    NewExpenseType controller = modal.getController();
                    controller.init(typesTable.getItems(), expenseType);
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        removeCol.setCellFactory(c -> new TableCell<ExpenseType, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                link.setOnAction(e -> {
                    typesTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        typesTable.setItems(FXCollections.observableArrayList(expenseUtil.allExpenseTypes()));
    }

    public void newType(ActionEvent event) {
        FXMLLoader modal = ViewUtility.modal("popups/newExpenseType", "New Type", true);
        NewExpenseType controller = modal.getController();
        controller.init(typesTable.getItems());
    }
}
