package controller.popups;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Good;
import model.Product;
import model.Requisition;
import utility.Transfer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IssueCommand {
    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Double> inStockCol;
    @FXML private TableColumn<Good, Number> removeCol;
    @FXML private TableColumn<Good, Number> editCol;

    @FXML private ChoiceBox<Product> product;
    @FXML private TextField quantity;
    @FXML private TextField requested;

    private Requisition requisition = null;
    private utility.Product productUtil = new utility.Product();
    private Good selectedGood = null;
    private Transfer transferUtil = new Transfer();
    private List<model.Transfer> requestTable;

    public void initialize(){
        product.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        product.setItems(FXCollections.observableArrayList(productUtil.all()));

        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double qty = Double.parseDouble(newValue);
            }catch (Exception ex){
                quantity.setText(oldValue);
            }
        });

        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, inStockCol, removeCol, editCol), Arrays.asList("product", "quantity", "instock", "id", "id"));
        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        inStockCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        removeCol.setCellFactory(c -> new TableCell<Good, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                link.setOnAction(e -> {
                    goodsTable.getItems().remove(getTableRow().getIndex());
                });
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        editCol.setCellFactory(c -> new TableCell<Good, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink hyperlink = new Hyperlink("Edit");
                hyperlink.setTextFill(Color.GREEN);
                setAlignment(Pos.BASELINE_CENTER);
                hyperlink.setOnAction(e -> {
                    Good good = goodsTable.getItems().get(getTableRow().getIndex());
                    product.getSelectionModel().select(good.getProduct());
                    requested.setText(GlobalValues.getDefaultDecimalFormat().format(good.getQuantity()));
                    selectedGood = good;
                });
                setGraphic(item == null || empty ? null : hyperlink);
            }
        });

        goodsTable.setItems(FXCollections.observableArrayList());
    }

    public void init(Requisition requisition, List<model.Transfer> items) {
        this.requisition = requisition;
        this.requestTable = items;
        if (this.requisition != null) {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(requisition.getGoods());
        }
    }

    public void edit(){

        Product selectedItem = product.getSelectionModel().getSelectedItem();

        if (selectedItem == null) {
            ViewUtility.alert("Oops", "You need to select a product to add to the goods requested or click edit to edit a good item");
            return;
        } else if (quantity.getText().trim().isEmpty()) {
            ViewUtility.alert("Oops", "Quantity must not be zero, if you want to add it");
            return;
        }

        double qty = Double.parseDouble(quantity.getText());

        Good good = transferUtil.createGood(null, selectedItem,  qty == 0 ? selectedGood.getQuantity() : qty, 0, 0);

        List<Good> items = goodsTable.getItems();

        for (Good item : items) {
            if (item.getProduct().getName().equalsIgnoreCase(good.getProduct().getName())) {
                good.setInstock(item.getInstock());
                good.setQuantityRequested(item.getQuantityRequested());
                goodsTable.getItems().remove(item);
                goodsTable.getItems().add(good);
                clearForm();
                return;
            }
        }


        goodsTable.getItems().add(good);
    }

    public void clearForm(){
        product.getSelectionModel().clearSelection();
        quantity.clear();
        requested.clear();
    }

    public void approve(){
        List<Good> goods = goodsTable.getItems();
        model.IssueCommand issueCommand = transferUtil.createIssueCommand(this.requisition);
        List<Good> goodList = new ArrayList<>();
        for (Good good : goods) {
            goodList.add(transferUtil.createGood(issueCommand, good.getProduct(), good.getQuantity(), good.getInstock(), good.getQuantityRequested()));
        }
//        requestTable.remove(this.requisition);
//        requestTable.add(issueCommand);
        DBManager.begin();
        issueCommand.setGoods(goodList);
        DBManager.commit();
        ViewUtility.closeWindow(quantity);
        ViewUtility.alert("Success", "Requisition has been approved");
    }
}
