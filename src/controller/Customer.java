package controller;


import controller.popups.RegisterCustomer;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import utility.Customers;

import java.sql.Date;
import java.util.Arrays;

public class Customer {
    @FXML private TableView<model.Customer> customersTable;
    @FXML private TableColumn<model.Customer, Date> registeredCol;
    @FXML private TableColumn<model.Customer, String> nameCol;
    @FXML private TableColumn<model.Customer, String> phoneCol;
    @FXML private TableColumn<model.Customer, String> addressCol;
    @FXML private TableColumn<model.Customer, Double> totalSalesCol;
    @FXML private TableColumn<model.Customer, Date> lastDateCol;
    @FXML private TableColumn<model.Customer, Number> editCol;
    @FXML private TableColumn<model.Customer, Number> removeCol;

    private Customers customerUtil = new Customers();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(registeredCol, nameCol, phoneCol, addressCol, totalSalesCol, lastDateCol, editCol, removeCol),
                Arrays.asList("dateRegistered", "fullName", "phone", "address", "totalSales", "lastSaleDate", "id", "id"));

        registeredCol.setCellFactory(c -> new TableCell<model.Customer, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        totalSalesCol.setCellFactory(c -> new TableCell<model.Customer, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        lastDateCol.setCellFactory(c -> new TableCell<model.Customer, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
               if (empty) {
                   setText(null);
               } else {
                   setText(item == null ? "No Sales made Yet" : GlobalValues.getDateInstance().format(item));
               }
            }
        });

        editCol.setCellFactory(c -> new TableCell<model.Customer, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/registerCustomer", "Register a new Customer");
                    RegisterCustomer controller = modal.getController();
                    controller.init(customersTable.getItems(), customersTable.getItems().get(getTableRow().getIndex()));
                });
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });

        removeCol.setCellFactory(c -> new TableCell<model.Customer, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                setAlignment(Pos.BASELINE_CENTER);
                link.setOnAction(e -> {
                    customersTable.getItems().remove(getTableRow().getIndex());
                });

                setGraphic(item == null || empty ? null : link);
            }
        });

        customersTable.setItems(FXCollections.observableArrayList(customerUtil.all()));
    }

    public void registerCustomer(){
        FXMLLoader modal = ViewUtility.modal("popups/registerCustomer", "Register a new Customer");
        RegisterCustomer controller = modal.getController();
        controller.init(customersTable.getItems());
    }
}
