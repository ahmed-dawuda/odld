package controller;


import controller.popups.AddBank;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Account;

import java.util.Arrays;

public class Bank {

    @FXML private TableView<Account> banksTable;
    @FXML private TableColumn<Account, String> banknameCol;
    @FXML private TableColumn<Account, String> branchCol;
    @FXML private TableColumn<Account, String> accnameCol;
    @FXML private TableColumn<Account, String> accnumberCol;
    @FXML private TableColumn<Account, Number> editCol;
    @FXML private TableColumn<Account, Number> removeCol;

    private utility.Account accountUtil = new utility.Account();


    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(banknameCol, branchCol, accnameCol, accnumberCol, editCol, removeCol), Arrays.asList("bank", "branch", "name", "number", "id", "id"));
        accnumberCol.setCellFactory(c -> new TableCell<Account, String>(){
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item);
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        editCol.setCellFactory(c -> new TableCell<Account, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/addBank", "Add New Bank Detail");
                    AddBank controller = modal.getController();
                    controller.init(banksTable.getItems(), banksTable.getItems().get(getTableRow().getIndex()));
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        removeCol.setCellFactory(c -> new TableCell<Account, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                link.setOnAction(e -> {
                    banksTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        banksTable.setItems(FXCollections.observableArrayList(accountUtil.all()));
    }

    public void addBank(){
        FXMLLoader modal = ViewUtility.modal("popups/addBank", "Add New Bank Detail");
        AddBank controller = modal.getController();
        controller.init(banksTable.getItems());
    }
}
