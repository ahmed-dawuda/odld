package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.CreditSaleInvoice;
import model.Customer;

import java.sql.Date;
import java.util.List;

public class Customers {
    public List<Customer> all(){
        return DBManager.listAll(Customer.class);
    }

    public Customer createCustomer(String name, String phone, String address){
        Customer customer = new Customer();
        customer.setId(GlobalValues.generateId());
        customer.setAddress(address);
        customer.setDateRegistered(new Date(System.currentTimeMillis()));
        customer.setFullName(name);
        customer.setPhone(phone);
        DBManager.save(Customer.class, customer);
        return customer;
    }

    public void updateSale(Customer customer, CreditSaleInvoice invoice) {
        DBManager.begin();
        customer.setLastSaleDate(new Date(System.currentTimeMillis()));
        customer.setTotalSales(customer.getTotalSales() + invoice.getPayable());
        customer.getCreditsales().add(invoice);
        DBManager.commit();
    }
}
