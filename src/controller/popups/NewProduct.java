package controller.popups;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Category;
import model.Product;
import utility.Inventory;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.List;

public class NewProduct {

    @FXML private TextField productTF;
    @FXML private TextField priceTF;
    @FXML private TextField code;
    @FXML private TextField wholesale;

    @FXML private ChoiceBox<Category> category;

    private utility.Product productUtil = new utility.Product();

    private Inventory inventoryUtil = new Inventory();
    private utility.Category categoryUtil = new utility.Category();

    private Product editable = null;

    public void initialize(){
        priceTF.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try{
                double qty = Double.parseDouble(newValue);
            }catch (Exception ex){
                priceTF.setText(oldValue);
            }
        });

        wholesale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double p = Double.parseDouble(newValue);
            }catch (Exception e){
                wholesale.setText(oldValue);
            }
        });

        category.setConverter(new StringConverter<Category>() {
            @Override
            public String toString(Category object) {
                return object.getName();
            }

            @Override
            public Category fromString(String string) {
                return null;
            }
        });

        category.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            code.setText(newValue.getAbbreviation());
        });

        category.setItems(FXCollections.observableArrayList(categoryUtil.all()));
        category.getSelectionModel().selectFirst();
    }

    public void initEdit(Product product){
        editable = product;
        productTF.setText(product.getName());
        priceTF.setText(GlobalValues.getDefaultDecimalFormat().format(product.getPrice()));
        wholesale.setText(GlobalValues.getDefaultDecimalFormat().format(product.getWholesale()));
        category.getSelectionModel().select(product.getCategory());
        code.setText(product.getCode());
    }

    public void init(Product... product){
        if (product.length > 0) {
            initEdit(product[0]);
        }
    }

    public void addProduct(){
        if (productTF.getText().trim().isEmpty()) {
            ViewUtility.alert("Product name required", "You have not provided the product name, please provide it");
            return;
        } else if (priceTF.getText().trim().isEmpty()) {
            ViewUtility.alert("Product name required", "You have not provided the product name, please provide it");
            return;
        } else if (category.getValue() == null) {
            ViewUtility.alert("Category is required", "You have not provided the Category for this product, please provide it");
            return;
        } else if (wholesale.getText().isEmpty()) {
            ViewUtility.alert("Wholesale price is required", "You have not provided the wholesale price for this product, please provide it");
            return;
        } else if (code.getText().equalsIgnoreCase(category.getValue().getAbbreviation())) {
            ViewUtility.alert("Product Code required", "Please you have not provided the product code");
            return;
        }

        Product productByCode = productUtil.getProductByCode(code.getText().toUpperCase());

        if (productByCode != null) {
            ViewUtility.alert("Product Code already exist", "Sorry, this code already assigned to "+ productByCode.getName());
            return;
        }

        if (editable != null) {
            DBManager.begin();
            editable.setName(productTF.getText().trim());
            editable.setPrice(Double.parseDouble(priceTF.getText().trim()));
            editable.setUpdated_at(new Timestamp(System.currentTimeMillis()));
            editable.setCategory(category.getValue());
            editable.setWholesale(Double.parseDouble(wholesale.getText()));
            DBManager.commit();
            ViewUtility.closeWindow(priceTF);
        } else {
            Product product = productUtil.create(code.getText().toUpperCase(), category.getValue(), productTF.getText().trim(), Double.parseDouble(priceTF.getText().trim()), Double.parseDouble(wholesale.getText()));
            DBManager.begin();
            category.getValue().getProducts().add(product);
            DBManager.commit();

//            productsTable.add(product);
            inventoryUtil.initializeInventory(product);
            ViewUtility.closeWindow(priceTF);
        }
    }
}
