package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.ViewUtility;
import model.Account;
import utility.Payment;

import java.util.ArrayList;
import java.util.List;

public class NewPayment {
    @FXML private ChoiceBox<Account> account;
    @FXML private TextField amount;

    @FXML private Label accountname;
    @FXML private Label bankname;
    @FXML private Label branch;
    @FXML private Label accountnumber;

    private Account acc = null;

    /**
     * creating utility classes
     */
    private utility.Account accountUtil = new utility.Account();
    private Payment paymentUtil = new Payment();

    private List<model.Payment> payments = new ArrayList<>();

    public void initialize(){

        account.setConverter(new StringConverter<Account>() {
            @Override
            public String toString(Account object) {
                return object.getBank() + " - " + object.getBranch();
            }

            @Override
            public Account fromString(String string) {
                return null;
            }
        });

        account.getSelectionModel().selectedItemProperty().addListener((observable, oldSelection, newSelection) -> {
            accountname.setText(newSelection.getName());
            bankname.setText(newSelection.getBank());
            branch.setText(newSelection.getBranch());
            accountnumber.setText(newSelection.getNumber());
            acc = newSelection;
            System.out.println(acc.getPayments().toString());
        });


        account.setItems(FXCollections.observableArrayList(accountUtil.all()));
        account.getSelectionModel().selectFirst();
    }

    public void pay(){
        if (acc == null ) {
            ViewUtility.alert("Missing data", "Please select the bank account you're making payment to");
        } else if(amount.getText().isEmpty()) {
            ViewUtility.alert("Missing data", "Please specify the amount you wish to pay.");
        } else {
            double amt;
            try {
                amt = Double.parseDouble(amount.getText());
                model.Payment payment = paymentUtil.create(acc, amt);
                accountUtil.update(acc, payment);
                payments.add(payment);
                ViewUtility.alert("Success", "GHS " + amt + " has been paid to " + acc.getBank() + ", " + acc.getBranch() + " branch");
            } catch (Exception ex) {
                ViewUtility.alert("Invalid amount", "The amount you entered is not a number, please enter a number value");
                ex.printStackTrace();
            }
            ViewUtility.closeWindow(amount);
        }
    }

    public void init(List<model.Payment> payments){
        this.payments = payments;
    }

}
