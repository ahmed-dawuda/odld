package controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class DashboardManagement {
    @FXML private VBox content;

    public void initialize() throws IOException {
        loadGraph("salestats");
    }

    public void changeRequisition(ActionEvent event) throws IOException {
        String graph = ((Button) event.getSource()).getId().toLowerCase();
        loadGraph(graph);
    }

    public void loadGraph(String filename) throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + filename + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
