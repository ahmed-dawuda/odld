package controller.popups;


import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import mStuff.ViewUtility;
import model.Account;

import java.util.List;

public class AddBank {

    @FXML private TextField bank;
    @FXML private TextField branch;
    @FXML private TextField name;
    @FXML private TextField number;

    private List<Account> banksTable;
    private utility.Account accountUtil = new utility.Account();
    private Account account = null;

    public void initialize(){
        number.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) return;
            try {
                double n = Double.parseDouble(newValue);
            } catch (Exception ex){
                number.setText(oldValue);
            }
        });
    }

    public void init(List<Account> list, Account... accounts){
        banksTable = list;
        if (accounts.length > 0) {
            this.account = accounts[0];
            bank.setText(this.account.getBank());
            branch.setText(this.account.getBranch());
            name.setText(this.account.getName());
            number.setText(this.account.getNumber());
        }
    }

    public void save(){
        if (bank.getText().isEmpty()) {
            ViewUtility.alert("Bank name required", "You must provide the name of the bank");
            return;
        } else if (branch.getText().isEmpty()) {
            ViewUtility.alert("Branch is required", "You must provide the branch for this bank");
            return;
        } else if (number.getText().isEmpty()) {
            ViewUtility.alert("Account Number is required", "Please provide the account number");
            return;
        } else if (name.getText().isEmpty()) {
            ViewUtility.alert("Account name is required", "The account name can not be empty. Please specify a value for it");
            return;
        } else if (this.account != null){
            Account account1 = accountUtil.updateAccount(this.account, bank.getText().trim(), branch.getText().trim(), name.getText().trim(), number.getText().trim());
            banksTable.remove(this.account);
            banksTable.add(account1);
            ViewUtility.closeWindow(name);
            ViewUtility.alert("Success", "Account Editing successful");
        } else {
            Account account = accountUtil.create(bank.getText().trim(), branch.getText().trim(), name.getText().trim(), number.getText().trim());
            banksTable.add(account);
            ViewUtility.closeWindow(name);
            ViewUtility.alert("Success", "Bank detail has been added");
        }
    }
}
