package controller.requisition;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;

import javax.persistence.Table;
import java.sql.Date;
import java.util.Arrays;

public class IssuedGoods {
    @FXML private TableView<Issue> requestTable;
    @FXML private TableColumn<Issue, Date> dateCol;
    @FXML private TableColumn<Issue, Zone> zoneCol;
    @FXML private TableColumn<Issue, Transfer> statusCol;
    @FXML private TableColumn<Issue, Transfer> dateRequestedCol;
    @FXML private TableColumn<Issue, Worker> workerCol;


    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Double> instockCol;

    private utility.Transfer transferUtil = new utility.Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, zoneCol, statusCol, dateRequestedCol, workerCol), Arrays.asList("date", "zone", "originator", "originator", "worker"));
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, instockCol), Arrays.asList("product", "quantity", "instock"));

        workerCol.setCellFactory(c -> new TableCell<Issue, Worker>(){
            @Override
            protected void updateItem(Worker item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getFirstname() + " " + item.getSurname());
            }
        });

        dateCol.setCellFactory(c -> new TableCell<Issue, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });
        zoneCol.setCellFactory(c -> new TableCell<Issue, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        statusCol.setCellFactory(c -> new TableCell<Issue, Transfer>(){
            @Override
            protected void updateItem(Transfer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : transferUtil.getStatus(item));
            }
        });

        dateRequestedCol.setCellFactory(c -> new TableCell<Issue, Transfer>(){
            @Override
            protected void updateItem(Transfer item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item.getDate()));
            }
        });

        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        instockCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(newValue.getGoods());
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.issues()));
        goodsTable.setItems(FXCollections.observableArrayList());
        requestTable.getSelectionModel().selectFirst();
    }
}
