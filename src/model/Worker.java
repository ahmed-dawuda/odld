package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "workers")
public class Worker {
    @Id
    @Column(name = "id")
    private Number id;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "othername")
    private String othername;

    @Column(name = "surname")
    private String surname;

    @Column(name = "level")
    private int level;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "password")
    private String password;

    @Column(name = "token")
    private String token;

    @OneToOne
    @JoinColumn(name = "primaryLocation")
    private Location primaryLocation;

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Location> locations = new ArrayList<>();

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Debit> debits = new ArrayList<>();

    @OneToMany(mappedBy = "worker")
    private List<Credit> credits;

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Payment> payments = new ArrayList<>();

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Invoice> invoices  = new ArrayList<>();

    @OneToMany(mappedBy = "worker", fetch = FetchType.LAZY)
    private List<Location> zones = new ArrayList<>();

    @OneToMany(mappedBy = "worker")
    private List<Expense> expenses = new ArrayList<>();

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public Location getPrimaryLocation() {
        return primaryLocation;
    }

    public void setPrimaryLocation(Location primaryLocation) {
        this.primaryLocation = primaryLocation;
    }

    public List<Location> getZones() {
        return zones;
    }

    public void setZones(List<Location> zones) {
        this.zones = zones;
    }

    public Number getId() {
        return id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public List<Debit> getDebits() {
        return debits;
    }

    public void setDebits(List<Debit> debits) {
        this.debits = debits;
    }

    public List<Credit> getCredits() {
        return credits;
    }

    public void setCredits(List<Credit> credits) {
        this.credits = credits;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    @Override
    public String toString() {
        return firstname + " " + othername + " " + surname;
    }
}
