package utility;

import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Inventory;
import model.Product;
import model.Worker;

import java.util.List;

public class Warehouse {
    utility.Inventory inventoryUtil = new utility.Inventory();

    public List<model.Warehouse> all(){
        return DBManager.listAll(model.Warehouse.class);
    }

    public model.Warehouse create(String name, model.Worker worker){
        model.Warehouse warehouse = new model.Warehouse();
        warehouse.setId(GlobalValues.generateId());
        warehouse.setName(name);
        warehouse.setWorker(worker);
        DBManager.save(model.Warehouse.class, warehouse);

        if (worker != null) {
            DBManager.begin();
            worker.getLocations().add(warehouse);
            DBManager.commit();
        }

        List<Inventory> inventories = inventoryUtil.initInventoryForLocation(warehouse);
        DBManager.begin();
        warehouse.setInventory(inventories);
        DBManager.commit();
        return warehouse;
    }

    public model.Warehouse update(model.Warehouse warehouse, String name, Worker worker){
        DBManager.begin();
        warehouse.setName(name);
        warehouse.setWorker(worker);
        DBManager.commit();
        return  warehouse;
    }

    public model.Warehouse find(Number id) {
        return DBManager.find(model.Warehouse.class, id);
    }
}
