package utility;


import mStuff.DBManager;

import java.util.List;

public class Requisition {
    public List<model.Requisition> all(){
        return DBManager.listAll(model.Requisition.class);
    }
}
