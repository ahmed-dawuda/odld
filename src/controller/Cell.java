package controller;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.ViewUtility;
import model.Zone;

import java.util.Arrays;

public class Cell {

    @FXML private TableView<model.Cell> cellsTable;
    @FXML private TableColumn<model.Cell, String> nameCol;
    @FXML private TableColumn<model.Cell, model.Zone> zoneCol;
    @FXML private TableColumn<model.Cell, String> caretakerCol;
    @FXML private TableColumn<model.Cell, String> phoneCol;

    private utility.Cell cellUtil = new utility.Cell();


    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, zoneCol, caretakerCol, phoneCol), Arrays.asList("name", "zone", "caretaker", "phone"));
        zoneCol.setCellFactory(c -> new TableCell<model.Cell, Zone>(){
            @Override
            protected void updateItem(Zone item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        cellsTable.setItems(FXCollections.observableArrayList(cellUtil.all()));
    }
}
