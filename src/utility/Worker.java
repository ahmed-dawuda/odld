package utility;

import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.util.List;

public class Worker {

    private Location locationUtil = new Location();

    public model.Worker create(String firstname, String surname, String phone, String password, String othername, model.Location location) {

        String type = locationUtil.type(location.getId());
        int level = 0;
        if (type.equalsIgnoreCase("Warehouse")) {
            level = 1;
        } else if (type.equalsIgnoreCase("Zone")) {
            level = 2;
        }
        String email = firstname.toLowerCase().charAt(0) + surname.toLowerCase().replace(" ", "");
        model.Worker worker = new model.Worker();
        worker.setId(GlobalValues.generateId());
        worker.setEmail(email);
        worker.setFirstname(firstname);
        worker.setSurname(surname);
        worker.setLevel(level);
        worker.setPassword(password);
        worker.setPhone(phone);
        worker.setOthername(othername.isEmpty() ? "-" : othername);
        worker.setPrimaryLocation(location);
        worker.getLocations().add(location);
        DBManager.save(model.Worker.class, worker);
        DBManager.begin();
        location.setWorker(worker);
        DBManager.commit();
        return worker;
    }

    public List<model.Worker> all(){
        return DBManager.listAll(model.Worker.class);
    }

    public List<model.Worker> getWorkersByLevel(int level) {
        return DBManager.query(model.Worker.class, "Select w from Worker w where w.level = ?1", level);
    }

    public List<model.Worker> getUnassignedWorkerByLevel(int level) {
        return DBManager.query(model.Worker.class, "Select w from Worker w where w.level = ?1 and w.primaryLocation = ?2", level, null);
    }
}
