package controller;


import controller.popups.AddWorker;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.Location;
import utility.*;
import utility.Warehouse;

import java.util.Arrays;

public class Worker {

    @FXML private TableView<model.Worker> workersTable;
    @FXML private TableColumn<model.Worker, String> firstnameCol;
    @FXML private TableColumn<model.Worker, String> surnameCol;
    @FXML private TableColumn<model.Worker, String> othernameCol;
    @FXML private TableColumn<model.Worker, String> emailCol;
    @FXML private TableColumn<model.Worker, String> levelCol;
    @FXML private TableColumn<model.Worker, String> passwordCol;
    @FXML private TableColumn<model.Worker, Number> fireCol;
    @FXML private TableColumn<model.Worker, Number> editCol;

    @FXML private TableView<Location> locationsTable;
    @FXML private TableColumn<Location, String> locationCol;
    @FXML private TableColumn<Location, Number> typeCol;

    utility.Worker workerUtil = new utility.Worker();
   utility.Location locationUtil = new utility.Location();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(firstnameCol, surnameCol, othernameCol, emailCol, levelCol, passwordCol, editCol, fireCol),
                Arrays.asList("firstname", "surname", "othername", "email", "phone", "password", "id", "id"));

        ViewUtility.initTableColumn(locationCol, "name");
        ViewUtility.initTableColumn(typeCol, "id");

        typeCol.setCellFactory(c -> new TableCell<Location, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : locationUtil.type(item));
            }
        });


        editCol.setCellFactory(c -> new TableCell<model.Worker, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/addworker", "Add new worker");
                    AddWorker addWorkerController = modal.getController();
                    addWorkerController.init(workersTable.getItems(), workersTable.getItems().get(getTableRow().getIndex()));
                });
                link.setTextFill(Color.GREEN);
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        fireCol.setCellFactory(c -> new TableCell<model.Worker, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink hyperlink = new Hyperlink("Terminate");
                hyperlink.setOnAction(e -> {

                });
                hyperlink.setTextFill(Color.valueOf("#e65100"));
                setGraphic(item == null || empty ? null : hyperlink);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        workersTable.setItems(FXCollections.observableArrayList(workerUtil.all()));
        locationsTable.setItems(FXCollections.observableArrayList());

        workersTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            locationsTable.getItems().clear();
            locationsTable.getItems().addAll(newValue.getLocations());
        });

        workersTable.getSelectionModel().selectFirst();
    }

    public void addWorker() {
        if (DBManager.listAll(Location.class).size() < 1) {
            ViewUtility.alert("Oops, no offices found", "In other to create a worker, you must first create a warehouse or zone");
            return;
        }
        FXMLLoader modal = ViewUtility.modal("popups/addworker", "Add new worker");
        AddWorker addWorkerController = modal.getController();
        addWorkerController.init(workersTable.getItems());
    }

}
