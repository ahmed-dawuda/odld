package controller.dashboard.sales;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.PieChart;
import javafx.scene.control.ChoiceBox;
import mStuff.GlobalValues;
import utility.Sale;

import java.time.LocalDate;
import java.time.Month;

public class Monthly {
    @FXML private ChoiceBox<String> month;
    @FXML private ChoiceBox<Integer> year;
    @FXML private PieChart pieChart;

    private Sale saleUtil = new Sale();

    public void initialize(){
//        System.out.println(Month.valueOf("APRIL").getValue());
        month.setItems(FXCollections.observableArrayList(GlobalValues.months));
        month.getSelectionModel().selectFirst();
        year.setItems(FXCollections.observableArrayList());
        int currentYear = LocalDate.now().getYear();
        for (int i = currentYear; i > 2015; i--) {
            year.getItems().add(i);
        }
        year.getSelectionModel().selectFirst();
        makeChart();
    }

    public void makeChart(){
        int selectedMonth = Month.valueOf(month.getValue()).getValue();
        int selectedYear = year.getValue();
        ObservableList<PieChart.Data> sales = saleUtil.chartBySales(selectedMonth, selectedYear);
        pieChart.setData(sales);
    }
}
