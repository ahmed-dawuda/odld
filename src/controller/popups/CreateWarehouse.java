package controller.popups;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.ViewUtility;
import model.Warehouse;
import model.Worker;

import java.util.List;

public class CreateWarehouse {
    @FXML private TextField name;
    @FXML private ChoiceBox<Worker> worker;

    private utility.Worker workerUtil = new utility.Worker();
    private utility.Warehouse warehouseUtil = new utility.Warehouse();
    private List<Warehouse> warehouseTable;
    private Warehouse warehouse = null;

    public void initialize(){
        worker.setConverter(new StringConverter<Worker>() {
            @Override
            public String toString(Worker object) {
                return object.getFirstname() + " " + object.getOthername() + " " + object.getSurname();
            }

            @Override
            public Worker fromString(String string) {
                return null;
            }
        });
        worker.setItems(FXCollections.observableArrayList(workerUtil.getWorkersByLevel(1)));
//        worker.getSelectionModel().selectFirst();
    }

    public void init(List<Warehouse> warehouses, Warehouse... warehouseArray){
        this.warehouseTable = warehouses;
        if (warehouseArray.length > 0) {
            this.warehouse = warehouseArray[0];
            this.name.setText(warehouse.getName().trim());
            this.worker.getSelectionModel().select(warehouse.getWorker());
        }
    }

    public void create(){
        if (name.getText().trim().isEmpty()) {
            ViewUtility.alert("Name is required", "You must specify the name of the warehouse you wish to create.");
            return;
        }

        if (this.warehouse != null) {
            warehouseUtil.update(warehouse, name.getText().trim(), worker.getSelectionModel().getSelectedItem());
            quit();
        } else {
            this.warehouseTable.add(warehouseUtil.create(name.getText().trim(), worker.getSelectionModel().getSelectedItem()));
            quit();
        }
    }

    public void quit(){
        ViewUtility.closeWindow(name);
    }
}
